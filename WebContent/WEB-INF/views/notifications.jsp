<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="${pageContext.request.contextPath}/static/css/style.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Enter External User Request</title>

</head>
<body>
<c:if test="${not empty message}">
			<div class="message">${message}</div>
		</c:if>
		<c:if test="${not empty error}">
			<div class="error">${error}</div>
		</c:if>
		<c:if test="${not empty notify}">
			<div class="notify">${notify}</div>
		</c:if>
		<table>
<thead>
  <tr>
     <th>From</th>
  </tr>
 </thead>

<c:forEach var="request" items="${requests}">

<tr>

<sf:form method="post" action="${pageContext.request.contextPath}/secure/banknotifications" commandName="isr"><td>
From: ${request.internalUsername}<sf:input type="hidden" path="internalUsername" value="${request.internalUsername}"  readonly="true"/>
</td><c:if test ="${request.transactionalReview == 0 }"><td>
	Accept<input type="radio" name="access" value="approve">
	Deny <input type="radio" name="access" value="deny">
 <input type="submit" name="submit" value="transactionReview"/> 
</td></c:if>
 <c:if test ="${request.userDetails == 0 }"><td>
 <input type="submit" name="submit" value="userDetail"/><input type="submit" name="deny" value="userDetail"/></td>
</c:if>
<c:if test ="${request.accountDetails == 0 }"><td>
 <input type="submit" name="submit" value="accountDetail"/><input type="submit" name="deny" value="accountDetail"/></td>
</c:if>
 </sf:form>
 
</tr>
</c:forEach>
</table>

