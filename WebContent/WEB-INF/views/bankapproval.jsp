<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
    <%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Payment approved</title>

</head>
<body>
This payment is from verified user. Now send below message to merchant by encrypting...
<br><br><br><br><br><br>
<form method="post" action="${pageContext.request.contextPath}/secure/bankapproval"> 
 <input type="hidden" class="control" value="${pk1.paymentid}" name="paymentid" id="paymentid" />
<table>

<tr>
<td>
Merchant Encrypted message<input type="text" class="control" value="${pk1.message}" readOnly>

User Encrypted message<input type="text" class="control" value="${pk2.message}" readOnly>

Merchant message<input type="text" class="control" name="mmessage">
User message<input type="text" class="control" name="umessage" >

	<input type="submit" name="submit" value="Approve" class="control"/>
	<input type="submit" name="submit" value="Reject" class="control"/>
	
</td>
<tr>
</tr>

</table>
</form>
</body>

</html>