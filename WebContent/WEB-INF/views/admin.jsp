<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
    <%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="${pageContext.request.contextPath}/static/css/style.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
User Account Requests
<c:if test="${not empty message}">
			<div class="message">${message}</div>
		</c:if>
<table class="formtable">
<thead>
  <tr>
     <th>Username</th>
     <th>Email</th>
     <th>Role</th>
  </tr>
 </thead>

<c:forEach var="user" items="${disabledUser}">
<tr>
<td><c:out value="${user.username}"></c:out> </td>
<td><c:out value="${user.email}"></c:out></td>
<td><c:out value="${user.authority}"></c:out></td> 
<td><sf:form method="post" action="${pageContext.request.contextPath}/admin/newhome" commandName="user"> 
	<input type="hidden" name="enabled" id="enabled" value="${user.username}"/>
	<input type="submit" class="control" name="submit" value="Enable User"/><input type="submit" name="delete" value="Delete User" class="control"/>
</sf:form></td> 

</tr>
</c:forEach>
</table>

Locked User Accounts

<table class="formtable">
<thead>
  <tr>
     <th>Username</th>
     <th>Email</th>
     <th>Role</th>
  </tr>
 </thead>

<c:forEach var="user" items="${lockedUser}">
<tr>
<td><c:out value="${user.username}"></c:out> </td>
<td><c:out value="${user.email}"></c:out></td>
<td><c:out value="${user.authority}"></c:out></td> 
<td><sf:form method="post" action="${pageContext.request.contextPath}/admin/unlockuser" commandName="user"> 
	<sf:input type="hidden" path="enabled" name="enabled" id="enabled" value="${user.username}"/>
	<input type="submit" class="control" name="submit" value="Unlock User"/>
</sf:form></td> 

</tr>
</c:forEach>
</table>

</body>
</html>
