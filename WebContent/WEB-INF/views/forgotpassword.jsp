<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/static/css/style.css"
	rel="stylesheet" type="text/css" />
<title>Forgot Password</title>
</head>
<body>
<h2>Forgot your password</h2>

<sf:form method="post" action="${pageContext.request.contextPath}/forgotpassword/otp" commandName="otp">
<c:if test="${not empty message}">
			<div class="message">${message}</div>
		</c:if>
		
<c:if test="${not empty error}">
			<div class="error">${error}</div>
		</c:if>
<table class="formtable">
<tr><td><label for="username">Enter your username</label></td>
<td><input type="text" placeholder="enter your username" name="username" /></td></tr>
<tr><td><input type="submit" value="send"/></td></tr>

</table>
</sf:form>



</body>
</html>