<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<h2>Submit payment to user</h2>
<h4>Merchant Username : <input type="text" id="mechantusername" name="mechantusername" value="<c:out value="${mechantusername}"></c:out>" readOnly></input></h4>

<sf:form id="makePayment" name="input"  method="post" action="${pageContext.request.contextPath}/secure/signfile" commandName="makepayment">
<table class="formtable">

			<sf:input path="paymentid" type="hidden" class="control" htmlEscape="true"></sf:input>
			<tr>
				<td class="label">Encrypted String</td>
				<td>
					<div class="error">
						<sf:errors path="message">
						</sf:errors>
					</div>
					<sf:input type="text" id="message" class="control" 
						htmlEscape="true" readonly="true" readname="message" path="message"></sf:input>
				</td>
			</tr>
			
			<tr>
				<td class="label">copy above string and signed using utility</td>
				<td><div class="error">
						<sf:errors path="encmessage">
						</sf:errors>
					</div> <sf:input type="text" id="amount" class="control" htmlEscape="true"
						path="encmessage" name="encmessage"></sf:input></td>
						<tr>
				<td class="label">This is merchant encrypted string</td>
				<td>
					<div class="error">
						
					</div>
					<input type="text" id="msg1" class="control" 
						 name="msg1" value="${pb.message}" readOnly>
						 
				</td>
			</tr>
			
			<tr>
				<td class="label">This is merchant signed string</td>
				<td><div class="error">
						
					</div> <input type="text" id="msg2" class="control" 
						name="msg2" value="${pb.encryptedmessage}" ></td>
			
			
			<tr><td class="label"> </td><td><input type="submit" class="control" value="Submit"></td></tr>
	
</table>
</sf:form>


			
			
			
	



</body>
</html>