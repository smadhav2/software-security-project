<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/static/css/style.css"
	rel="stylesheet" type="text/css" />
<title>New Password</title>
</head>
<body>
	<h2>Enter new password</h2>

	<form method="post"
		action="${pageContext.request.contextPath}/login">
		${username}
		<table class="formtable">
			<tr>
				<td class="label">Password:</td>
					<td>	
					<input type="password" class="control" 
						name="password"/></td>

			</tr>
			<tr>
				<td class="label">Confirm Password:</td>
				<td><input type="password" class="control"
					name="confirm_password"></input></td>
				
			</tr>
			
			<tr>
				<td><input type="hidden" name="username" class ="control" id="username" value="${username}"/>
				<input type="submit" value="send" /></td>
			</tr>

		</table>
	</form>



</body>
</html>