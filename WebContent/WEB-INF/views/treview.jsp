
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


	
<c:forEach var="m" items="${map}">
  <b> Account Number:</b> ${m.key.accountNumber}   <b>Account Type:</b>  ${m.key.accountType} <b>Account Balance:</b> ${m.key.accountBalance}  <b>Available Balance:</b> ${m.key.availableBalance} 
   <table class="table">
		<thead>
			<tr>
				
				<th>Account No</th>
				<th>Type</th>
				<th>Involved Party</th>
				<th>Amount</th>
				<th>Status</th>
			</tr>
		</thead>
		
<c:if test="${not empty m.value}">
			
		
		<c:forEach items="${m.value}" var="trans" >
			<tr>

				<td><c:out value="${trans.accountNumber}"></c:out></td>
				<td><c:out value="${trans.type}"></c:out></td>
				<td><c:out value="${trans.otherparty}"></c:out></td>
				<td><c:out value="${trans.amount}"></c:out></td>
				<td><c:out value="${trans.status}"></c:out></td>
				<td><sf:form method="post" action="${pageContext.request.contextPath}/" commandName="trans"></sf:form></td>
			</tr>
		</c:forEach></c:if>
	</table>
</c:forEach>

</body>
</html>