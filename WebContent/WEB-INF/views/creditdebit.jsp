<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/static/css/style.css" rel="stylesheet" type="text/css" />
<title>Insert title here</title>
</head>
<body>
	This is a page for deposit...<br/>
  <c:if test="${not empty message}">
			<div class="message">${message}</div>
		</c:if>
		
<c:if test="${not empty error}">
			<div class="error">${error}</div>
		</c:if>
	<sf:form name="input"
		action="${pageContext.request.contextPath}/secure/atm"
		method="post" commandName="newTran">
		<table class="formtable"> 
		<tr>
    	<td>Account Number:</td>
    	<td>
        <sf:select path="accountNumber">
            <sf:option value="0" label="--Please Select"/>
            <sf:options items="${Accounts}" itemValue="accountNumber" itemLabel="accountType"/>
        </sf:select>
        
    </td></tr>
			
 			<tr> 
 				<td class="label">Amount:</td> 
				<td> <sf:input type="text" class="control" htmlEscape="true"
						path="Amount" name="Amount"></sf:input></td>
 			</tr> 

		<tr> 
				<td class="label">Type :</td> 
				<td> Debit : <sf:radiobutton path="Type" name ="Type" value="Debit" /></td>
				<td> Credit : <sf:radiobutton path="Type" name ="Type" value="Credit" /></td>
 			</tr> 
 			<tr> 
 				<td class="label"></td> 
				<td><input type="submit" class="control" value="Submit"></td> 
 			</tr> 
		</table> 
	</sf:form>

</body>
</html>