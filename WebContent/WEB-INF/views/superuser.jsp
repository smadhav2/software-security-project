<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Super USER</title>
</head>
<body>
	This is a page for SuperUser... Create An admin Here...

	<sf:form name="admin"
		action="${pageContext.request.contextPath}/registerAdmin"
		method="post" commandName="Auser">
		<table class="formtable">
			<tr>
				<td class="label">UserName:</td>
				<td><div class="error">
						<sf:errors path="username">
						</sf:errors>
					</div> <sf:input class="control" type="text" htmlEscape="true"
						path="username" name="username"></sf:input></td>
			</tr>
			<tr>
				<td class="label">Email:</td>
				<td>
					<div class="error">
						<sf:errors path="email">
						</sf:errors>
					</div>
					<sf:input type="text" class="control" path="email"
						htmlEscape="true" name="email"></sf:input>
				</td>
			</tr>
			<tr>
				<td class="label">Password:</td>
				<td><div class="error">
						<sf:errors path="password">
						</sf:errors>
					</div> <sf:input type="password" class="control" htmlEscape="true"
						path="password" name="password"></sf:input></td>
			</tr>
			<tr>
				<td class="label">Confirm Password:</td>
				<td><input type="password" class="control"
					name="confirm_password"></input></td>
			</tr>


			<tr>
				<td class="label"></td>
				<td><input type="submit" class="control" value="Submit"></td>
			</tr>
		</table>
	</sf:form>
<p><a href="${pageContext.request.contextPath}/j_spring_security_logout">Logout</a></p>
</body>
</html>