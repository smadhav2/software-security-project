<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="${pageContext.request.contextPath}/static/css/style.css"
	rel="stylesheet" type="text/css" />
<title>Forgot Password</title>
</head>
<body>
<h2>Enter otp send to your registered email</h2>

<sf:form method="post" action="${pageContext.request.contextPath}/forgotpassword/otp/newpassword" commandName="otp">
<table class="formtable">

<tr><td>User Name: <c:out value="${username}"> </c:out></td></tr>
<tr><td><label for="otp">Enter otp</label></td>
<td><input type="text" placeholder="enter otp" name="otp" /></td></tr>
<sf:input type="hidden" path="username" name="username" id="username" value="${username}"/>
<tr><td><input type="submit" value="Confirm"/></td></tr>

</table>
</sf:form>



</body>
</html>