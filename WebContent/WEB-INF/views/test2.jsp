<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
    <%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<link href="${pageContext.request.contextPath}/static/css/style.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Payment Request Status</title>
</head>
<body>

Payment Approval Requests
<h4>Username : <c:out value="${username}"></c:out></h4>
<c:if test="${not empty message}">
<div class="message">${message}</div>
</c:if>



<table class="formtable">
<thead>
  <tr>
	 <th>Payment ID</th>
     <th>User Name</th>
     <th>Amount</th>
     <th>Quantity</th>
     <th>Total Amount</th>
     <th>Description</th>

  </tr>
 </thead>
<tr> List of payments approved by user </tr>
<c:forEach var="user" items="${merchantusername}">
<tr>
<td><c:out value="${user.paymentid}"></c:out> </td>
<td><c:out value="${user.username}"></c:out> </td>
<td><c:out value="${user.amount}"></c:out></td>
<td><c:out value="${user.quantity}"></c:out></td>
<td><c:out value="${user.totalAmount}"></c:out></td>
<td><c:out value="${user.description}"></c:out></td>

<td></td>
<td><sf:form method="post" action="${pageContext.request.contextPath}/secure/newhome" commandName="recievepayment"> 
<input type="hidden" class="control" name="paymentid" id="paymentid" value="${user.paymentid}"/><
<input type="submit" class="control" name="Approve" value="Send to bank"/></sf:form>
</td> 
</tr>

</c:forEach>

</table>
<br><br>
<table class="formtable">
<thead>
  <tr>
	 <th>Payment ID</th>
     <th>User Name</th>
     <th>Amount</th>
     <th>Quantity</th>
     <th>Total Amount</th>
     <th>Description</th>
 
  </tr>
 </thead>
 
<tr> List of pending payments from bank</tr>
<c:forEach var="user" items="${pending}">
<tr>
<td><c:out value="${user.paymentid}"></c:out> </td>
<td><c:out value="${user.username}"></c:out> </td>
<td><c:out value="${user.amount}"></c:out></td>
<td><c:out value="${user.quantity}"></c:out></td>
<td><c:out value="${user.totalAmount}"></c:out></td>
<td><c:out value="${user.description}"></c:out></td>

</tr>
</c:forEach>
</table>
<br><br>
<table class="formtable">
<thead>
  <tr>
	 <th>Payment ID</th>
     <th>User Name</th>
     <th>Amount</th>
     <th>Quantity</th>
     <th>Total Amount</th>
     <th>Description</th>

  </tr>
 </thead>
 
<tr> List of payments rejected by user </tr>
<c:forEach var="user" items="${merchantusernamereject}">
<tr>
<td><c:out value="${user.paymentid}"></c:out> </td>
<td><c:out value="${user.username}"></c:out> </td>
<td><c:out value="${user.amount}"></c:out></td>
<td><c:out value="${user.quantity}"></c:out></td>
<td><c:out value="${user.totalAmount}"></c:out></td>
<td><c:out value="${user.description}"></c:out></td>

</tr>
</c:forEach>
</table>
<br><br>
<table class="formtable">
<thead>
  <tr>
	 <th>Payment ID</th>
     <th>User Name</th>
     <th>Amount</th>
     <th>Quantity</th>
     <th>Total Amount</th>
     <th>Description</th>
     
  </tr>
 </thead>
 
<tr> List of payments rejected by bank </tr>
<c:forEach var="user" items="${reject}">
<tr>
<td><c:out value="${user.paymentid}"></c:out> </td>
<td><c:out value="${user.username}"></c:out> </td>
<td><c:out value="${user.amount}"></c:out></td>
<td><c:out value="${user.quantity}"></c:out></td>
<td><c:out value="${user.totalAmount}"></c:out></td>
<td><c:out value="${user.description}"></c:out></td>

</tr>
</c:forEach>
</table>

</body>
</html>