<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/static/js/jquery-1.11.1.min.js"></script>
<title>Insert title here</title>
</head>
<body>
<!-- action="${pageContext.request.contextPath}/secure/submitpayment" oninput="message.value = 'User ' + username.value + ' owes ' + totalAmount.value + ' to ' + mechantusername.value" -->
<h2>Submit payment to user</h2>
<h4>Merchant Username : <input type="text" id="mechantusername" name="mechantusername" value="<c:out value="${mechantusername}"></c:out>" readOnly></input></h4>
<sf:form id="makePayment" oninput="message.value = 'User ' + username.value + ' owes ' + totalAmount.value + ' to ' + mechantusername.value" name="input"  method="post" action="${pageContext.request.contextPath}/secure/submitpayment" commandName="makepayment">
<table class="formtable">

			<tr>
				<td class="label">UserName:</td>
				<td><div class="error">
						<sf:errors path="username">
						</sf:errors>
					</div> 
					
					<%-- <sf:select class="control" htmlEscape="true"
						path="username" id="username" name="username">
						<c:forEach var="username" items="${username}">
							<option value="${username.username}">${username.username}</option>
						</c:forEach>
					</sf:select></td> --%>
					<td>
					<sf:select path="username">
            <sf:option value="0" label="--Please Select"/>
            <sf:options items="${username}" itemValue="username" itemLabel="username"/>
        </sf:select>
        </td>
			</tr>
			<tr>
				<td class="label">Description</td>
				<td>
					<div class="error">
						<sf:errors path="description">
						</sf:errors>
					</div>
					<sf:input type="text" id="description" class="control" path="description"
						htmlEscape="true" name="description"></sf:input>
				</td>
			</tr>
			<tr>
				<td class="label">Amount</td>
				<td><div class="error">
						<sf:errors path="amount">
						</sf:errors>
					</div> <sf:input type="text" id="amount" class="control" htmlEscape="true"
						path="amount" name="amount"></sf:input></td>
			</tr>
			
			<tr>
				<td class="label">Quantity</td>
				<td><div class="error">
						<sf:errors path="quantity">
						</sf:errors>
					</div> <sf:input type="text" id="quantity" class="control" htmlEscape="true"
						path="quantity" name="quantity"></sf:input></td>
			</tr>

			<% %>
			<tr>
				<td class="label">Total Amount</td>
				<td><div class="error">
						<sf:errors path="totalAmount">
						</sf:errors>
					</div> <sf:input type="text" class="control" htmlEscape="true"
						path="totalAmount" name="totalAmount" id="totalAmount"></sf:input></td>			
			</tr>
			<tr>
				<td class="label">Message</td>
				<td> <input type="text" class="control" id="message" htmlEscape="true" name="message"></input></td>
			</tr>
			
			
			<tr><td class="label"> </td><td><input type="submit" class="control" value="Encrypt Message"></td></tr>
	
</table>
</sf:form>
</body>
</html>
