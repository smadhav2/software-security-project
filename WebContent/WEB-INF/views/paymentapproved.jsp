<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
    <%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Payment approved</title>

</head>
<body>
This payment is from verified user. Now send below message to merchant by encrypting...
<br><br><br><br><br><br>
<sf:form method="post" action="${pageContext.request.contextPath}/secure/paymentapproval" commandName="payment"> 
 <sf:input type="hidden" class="control" path="paymentid" name="paymentid" id="paymentid" />
<table>
<tr>

<td> 
Decrypt this message from your private key using the jar utility
</td>

<td> 
<%-- <input type="text" size="100" name="description" value="${Transactionmessage}"> --%>
<sf:input type="text" readonly="true" path="message" name="message" id="message" class="control" ></sf:input>
</td>
<tr>
<td> Enter the decrypted value using your private key
</td>
<td> 
<input type="text"  name="plainmessage" id="plainmessage" class="control">
</td>
</tr>
<tr>
<td>


	<input type="submit" name="submit" value="Encrypt & Approve" class="control"/>
	<input type="submit" name="submit" value="Reject" class="control"/>
	
</td>
<tr>
</tr>

</table>
</sf:form>
</body>

</html>