<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
    <%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Insert title here</title>
</head>
<body>
<sf:form method="post" action="${pageContext.request.contextPath}/secure/paymentfromuser" commandName="payment" > 
<sf:input type="hidden" class="control" path="paymentid" name="paymentid" id="paymentid" />
This payment is from verified user. Now send below message to merchant by encrypting...
<tr>
<td>
<sf:input type="text" readonly="true" path="usermessage" name="usermessage" class="control" ></sf:input>
</td>
</tr>
<tr>
<td> Enter the signed value using your private key
</td>
<td> 
<sf:input type="text"  path="userencryptedmessage" name="userencryptedmessage" class="control"></sf:input>
</td>
</tr>
<input type="submit" name="approve" value="approve" class="control"></input>
</sf:form>
</body>
</html>