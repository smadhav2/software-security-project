<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="${pageContext.request.contextPath}/static/css/style.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Enter External User Request</title>

</head>
<body>
<c:if test="${not empty message}">
			<div class="message">${message}</div>
		</c:if>
<table class="formtable" >
<sf:form commandName="externalUserDetails" method="post" action="${pageContext.request.contextPath}/secure/updateuserdetails">

<tr>
    <td>FirstName:</td>
    <td>
        <sf:input class="control" type="text" htmlEscape="true" path ="firstName" name="firstName"></sf:input>
    </td>
</tr>
<tr>
    <td>MiddleName:</td>
    <td>
        <sf:input class="control" type="text" htmlEscape="true" path ="middleName" name="middleName"></sf:input>
    </td>
</tr>
<tr>
    <td>LastName:</td>
    <td>
        <sf:input class="control" type="text" htmlEscape="true" path ="lastName" name="lastName"></sf:input>
    </td>
</tr>
<tr>
    <td>Cell No:</td>
    <td>
        <sf:input class="control" type="text" htmlEscape="true" path ="cellNo" name="cellNo"></sf:input>
    </td>
</tr>
<tr>
    <td>SSN:</td>
    <td>
        <sf:input class="control" type="password" htmlEscape="true" path ="ssn" name="ssn"></sf:input>
    </td>
</tr>
<tr>
    <td>Address:</td>
    <td>
        <sf:textarea class="control" rows="1" cols="10" htmlEscape="true" path ="address" name="address"></sf:textarea>
    </td>
</tr>
<tr>
    <td>City:</td>
    <td>
        <sf:input class="control" type="text" htmlEscape="true" path ="city" name="city"></sf:input>
    </td>
</tr>
<tr>
    <td>Zipcode</td>
    <td>
        <sf:input class="control" type="text" htmlEscape="true" path ="zipcode" name="zipcode"></sf:input>
    </td>
</tr> 
<tr>
    <td>State:</td>
    <td>
        <sf:input class="control" type="text" htmlEscape="true" path ="state" name="state"></sf:input>
    </td>
</tr>
<tr>
<td></td>
<td><input type="submit" value="save details"/></td>
</tr>

</sf:form>
</table>

</body>
</html>