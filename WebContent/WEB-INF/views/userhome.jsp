<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
<link href="${pageContext.request.contextPath}/static/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/static/js/jquery.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Welcome to your home page</title>
</head>
<body>
<h2>Welcome to your home page <c:out value="${user.username}"></c:out></h2>

<c:if test="${not empty message}">
			<div class="message">${message}</div>
		</c:if>

<div class="">
		<sec:authorize access="hasRole('ROLE_EXTERNALUSER')">
		Username : <c:out value="${username}"></c:out>
		
		
		<p><a href="<c:url value='/secure/newhome'/>">New Test Page</a></p>
		<p><a href="<c:url value='/secure/createaccount'/>">Account Management</a></p>
		<p><a href="<c:url value='/secure/viewTransaction'/>">Account Summary</a></p>
		<p><a href="<c:url value='/secure/atm'/>">ATM</a></p>
		<p><a href="<c:url value='/secure/transfer'/>">Go to Transfers..</a></p>
		<p><a href="<c:url value='/secure/updateuserdetails'/>">Enter User Details</a></p>

		<p><a href="<c:url value='/secure/banknotifications'/>">Notifications</a></p>
		

		<p><a href="<c:url value='/secure/paymentapproval'/>">Payment Approval Requests</a></p>

		
		</sec:authorize>
	</div>
	<div class="">
		<sec:authorize access="hasRole('ROLE_INTERNALUSER')">
		Username : <c:out value="${username}"></c:out>
		
		<p><a href="<c:url value='/secure/updateinuserdetails'/>">Enter User Details</a></p>
		<p><a href="<c:url value='/secure/paymentfrommerchant'/>">Payment Requests from merchant</a></p>
		
		
		</sec:authorize>
	</div>
	<div class="">
		<sec:authorize access="hasRole('ROLE_ADMIN')">
		Username : <c:out value="${username}"></c:out>
		<p><a href="<c:url value='/admin/newhome'/>">UserRequests (<span id="requests">0</span>)</a></p>
		<p><a href="<c:url value='/admin/register'/>">Create New Internal User</a></p>
		<p><a href="<c:url value='/admin/makerequests'/>">Make User Requests</a></p>
		
		</sec:authorize>
	</div>
<div class="">
		<sec:authorize access="hasRole('ROLE_MERCHANT')">
		Username : <c:out value="${username}"></c:out>
		
		<p><a href="<c:url value='/secure/newhome'/>">Payment Request Status</a></p>
		<p><a href="<c:url value='/secure/makepayment'/>">Submit Payment To User</a></p>
		</sec:authorize>
	</div>
<p><a href="${pageContext.request.contextPath}/j_spring_security_logout">Logout</a></p>

<script>
function onLoad(){
	$.getJSON("<c:url value="/getmessages"/>",updateUserRequests);
}
function updateUserRequests(data){
	$("#requests").text(data.size);
}
$(document).ready(onLoad);

</script>
</body>
</html>
