<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="${pageContext.request.contextPath}/static/css/style.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Account Management Page</title>

</head>
<body>
<h2>Create Your Personal or Saving Account</h2>

<sf:form name="input" action="${pageContext.request.contextPath}/secure/createaccount" method="post" commandName="account">
<table class="formtable">
<c:if test="${not empty message}">
			<div class="message">${message}</div>
		</c:if>
		
<c:if test="${not empty error}">
			<div class="error">${error}</div>
		</c:if>
<tr><td class="label">Account Type: </td><td> <div class="error"><sf:errors path = "accountType" > </sf:errors></div> 
    
        Savings: <sf:radiobutton path="accountType" value="Savings"/> 
        Checkings: <sf:radiobutton path="accountType" value="Checkings"/>
    </td> </tr>
<tr><td class="label">Account Balance: </td><td><div class="error"><sf:errors path = "accountBalance"> </sf:errors></div> <sf:input type="text" class="control" htmlEscape="true" path="accountBalance" name="accountBalance"></sf:input> </td></tr>


<tr><td class="label"> </td><td><input type="submit" class="control" value="Submit"></td></tr>
</table>
</sf:form>
</body>
</html>