<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="${pageContext.request.contextPath}/static/css/style.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create Internal User Request</title>

</head>
<body>
<c:if test="${not empty message}">
			<div class="message">${message}</div>
			
		</c:if>
<c:if test="${not empty error}">
			<div class="error">${error}</div>
			
		</c:if>
<table class="formtable" >
<sf:form commandName="internalUserRequest" method="post" action="${pageContext.request.contextPath}/admin/makerequests">
<tr>
    <td>UserList:</td>
    <td>
        <sf:select path="externalUsername">
            <sf:option value="-" label="--Please Select"/>
            <sf:options items="${externalUserList}" itemValue="username" itemLabel="username"/>
        </sf:select>
    </td>
    
</tr>
<tr>
    <td></td>
    <td>
        Transactional Review: <input type="submit" name="transactionReview" value="transactionReview"/> <br/>
        User Details: <input type="submit" name="transactionReview" value="userDetail"/><br/>
        Account Details: <input type="submit" name="transactionReview" value="accountDetail"/>
    </td>
</tr>
<tr>
<td></td>

</tr>

</sf:form>
</table>
<table>
<thead><tr>
				
				<th>Username</th>
				<th>Transaction Review</th>
				<th>User Details</th>
				<th>Account Details</th>
				
			</tr></thead>
<c:forEach var="access" items="${accessList}">
<tr>
<td><c:out value="${access.externalUsername}"></c:out> </td>
<td>
<c:if test="${access.transactionalReview == 1}"><c:out value="Approved"></c:out></c:if>
<c:if test="${access.transactionalReview == 0}"><c:out value="Pending"></c:out></c:if>
<c:if test="${access.transactionalReview == 2}"><c:out value="Denied"></c:out></c:if> </td>
<td>
<c:if test="${access.userDetails == 1}"><c:out value="Approved"></c:out></c:if>
<c:if test="${access.userDetails == 0}"><c:out value="Pending"></c:out></c:if>
<c:if test="${access.userDetails == 2}"><c:out value="Pending"></c:out></c:if>
 </td>
<td>
<c:if test="${access.accountDetails == 1}"><c:out value="Approved"></c:out></c:if>
<c:if test="${access.accountDetails == 0}"><c:out value="Pending"></c:out></c:if>
<c:if test="${access.accountDetails == 2}"><c:out value="Pending"></c:out></c:if> 
</td>
</tr>
</c:forEach>
</table>

</body>
</html>