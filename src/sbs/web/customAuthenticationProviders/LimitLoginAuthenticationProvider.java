package sbs.web.customAuthenticationProviders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import sbs.web.dao.UserAttempts;
import sbs.web.service.UserAttemptsService;
import sbs.web.service.UserService;

@Component("authProvider")
public class LimitLoginAuthenticationProvider extends DaoAuthenticationProvider {
	
	@Autowired
	private UserAttemptsService userAttemptsService;
	
	@Autowired
	private UserService userService;

	
	@Autowired
	@Qualifier("loginAuthenticationBo")
	public void setUserDetailsService(UserDetailsService userDetailsService) {
		super.setUserDetailsService(userDetailsService);
	}
	
	@Autowired
	@Qualifier("passwordEncoder")
	public void setPasswordEncoder(Object passwordEncoder) {
		// TODO Auto-generated method stub
		super.setPasswordEncoder(passwordEncoder);
	}

	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		// TODO Auto-generated method stub
		try{
			Authentication auth = super.authenticate(authentication);
			if(userAttemptsService.getNumberOfUserAttempts(auth.getName()) < 3 && userAttemptsService.getNumberOfUserAttempts(auth.getName()) != -1){
				
			userAttemptsService.resetFailAttempts(auth.getName());
			}
			else if(userAttemptsService.getNumberOfUserAttempts(auth.getName()) >= 3){
				userAttemptsService.lockUserAccount(authentication.getName());
				throw new LockedException("User Account has been locked");
			}
			return auth;
			
		}
		catch(BadCredentialsException e){
				try{
					userAttemptsService.updateFailAttempts(authentication.getName());
					if(userAttemptsService.getNumberOfUserAttempts(authentication.getName()) >= 3){
						userAttemptsService.lockUserAccount(authentication.getName());
						}
					throw new BadCredentialsException("Username and Password do not match");
				}
				catch(BadCredentialsException ex){
					throw ex;
				}
		}
		
			 catch(LockedException ex){
			//this user is locked!
				 
			String error = "";
			
			UserAttempts userAttempts = 
	                    userAttemptsService.getUserAttempts(authentication.getName());
	 
	               if(userAttempts!=null){
				
	       			
				error = "User account is locked! <br><br>Username : " 
	                           + authentication.getName() + "<br>Last Attempts : ";
			}else{
				error = ex.getMessage();
			}
	               throw new LockedException(error);
	 
			 }
		
			
		
	}
	
	

}
