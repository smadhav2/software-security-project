
package sbs.web.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;



@Entity(name="paymentDetails")
public class MakePayment {
	
	
	public MakePayment(int paymentid, String merchantusername, String username,
			int amount, int quantity, int totalAmount, String description,
			int approved, String encmessage, String message, String userencryptedmessage, String usermessage) {
		this.paymentid = paymentid;
		this.merchantusername = merchantusername;
		this.username = username;
		this.amount = amount;
		this.quantity = quantity;
		this.totalAmount = totalAmount;
		this.description = description;
		this.approved = approved;
		this.encmessage=encmessage;
		this.message=message;
		this.userencryptedmessage=userencryptedmessage;
		this.usermessage=usermessage;
	}

	public MakePayment() {
		// TODO Auto-generated constructor stub
	}
	
	@GeneratedValue
	@Id
	@Column(name="paymentid")
	private int paymentid;
	
	@Column(name="merchantusername")
	private String merchantusername;
	
	public int getPaymentid() {
		return paymentid;
	}

	public void setPaymentid(int paymentid) {
		this.paymentid = paymentid;
	}

	@Column(name="username")
	private String username;
	
	@Column(name="amount")
	private int amount;
	
	@Column(name="quantity")
	private int quantity;
	
	@Column(name="totalAmount")
	private int totalAmount;
	
	@Column(name="description")
	private String description;
	
	@Column(name="approved")
	private int approved;
	
	@Column(name="encryptedmessage")
	private String encmessage;
	
	@Column(name="message")
	private String message;
	
	@Column(name="userencryptedmessage")
	private String userencryptedmessage;
	
	@Column(name="usermessage")
	private String usermessage;
	
	public String getUserencryptedmessage() {
		return userencryptedmessage;
	}

	public void setUserencryptedmessage(String userencryptedmessage) {
		this.userencryptedmessage = userencryptedmessage;
	}

	public String getUsermessage() {
		return usermessage;
	}

	public void setUsermessage(String usermessage) {
		this.usermessage = usermessage;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getEncmessage() {
		return encmessage;
	}

	public void setEncmessage(String encmessage) {
		this.encmessage = encmessage;
	}

	public String getMerchantusername() {
		return merchantusername;
	}

	public void setMerchantusername(String merchantusername) {
		this.merchantusername = merchantusername;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(int totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getApproved() {
		return approved;
	}

	public void setApproved(int approved) {
		this.approved = approved;
	}
}