package sbs.web.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sbs.web.dao.interfaces.InternalUserDetailsDao;
import sbs.web.util.HibernateDaoSupportCustomized;

@Transactional
@Repository("internalUserDetailsDao")
public class InternalUserDetailsDaoImpl extends HibernateDaoSupportCustomized implements InternalUserDetailsDao{
	
	
	
	@Override
	public InternalUserDetails getInternalUserDetails(String username) {
		Criteria crit = getSession().createCriteria(InternalUserDetails.class);
		crit.add(Restrictions.eq("username",username));
		if(crit.uniqueResult()!=null){
			return (InternalUserDetails) crit.uniqueResult();
		}
		
		return null;
	}

	@Override
	public boolean exists(String username) {
		Criteria crit = getSession().createCriteria(InternalUserDetails.class);
		crit.add(Restrictions.eq("username",username));
		if(crit.uniqueResult()!=null)
			return true;
		else
			return false;
		
	}

	@Override
	public void save(InternalUserDetails internalUserDetails) {
		getHibernateTemplate().save(internalUserDetails);
		
	}

	@Override
	public int getInternalUserId(String username) {
		Criteria crit = getSession().createCriteria(InternalUserDetails.class);
		crit.add(Restrictions.eq("username",username));
		if(crit.uniqueResult()!=null)
			return ((InternalUserDetails)crit.uniqueResult()).getInternaluserdetialid();
		
		return 0;
	}

	@Override
	public void update(InternalUserDetails internalUserDetails) {
		getHibernateTemplate().update(internalUserDetails);
		
	}
	

}
