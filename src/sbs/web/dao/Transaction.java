package sbs.web.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="transactions")
public class Transaction {
	
	@Id
	@GeneratedValue
	@Column(name="Tid")
	private int tid;
	
	@Column(name="AccountNumber")
	private int accountNumber;
	
	@Column(name="type")
	private String type;
	
	@Column(name="Otherparty")
	private int otherparty;
	
	@Column(name="Amount")
	private int amount;
	
	@Column(name="status")
	private String status;

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Transaction() {
		
	}
	public Transaction(int tid, int accountNumber, String type, int otherparty,
			int amount) {
				this.tid = tid;
				this.accountNumber = accountNumber;
				this.type = type;
				this.otherparty = otherparty;
				this.amount = amount;
	}

	public int getTid() {
		return tid;
	}

	public void setTid(int tid) {
		this.tid = tid;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getOtherparty() {
		return otherparty;
	}

	public void setOtherparty(int otherparty) {
		this.otherparty = otherparty;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	
	@Override
	public String toString() {
		return "Transaction [Tid=" + tid + ", AccountNumber=" + accountNumber
				+ ", Type=" + type + ", Otherparty=" + otherparty + ", Amount="
				+ amount + "]";
	}
	

}
