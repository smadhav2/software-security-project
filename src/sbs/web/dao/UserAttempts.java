package sbs.web.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

	@Entity
	@Table(name = "user_attempts")
	public class UserAttempts {
		 
		@Id
		private int id;
		
		@Column(name = "username")
		private String username;
		@Column(name="attempts")
		private int attempts;
		
		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public int getAttempts() {
			return attempts;
		}
		public void setAttempts(int attempts) {
			this.attempts = attempts;
		}
		
		//getter and setter
	 
	}
	

