package sbs.web.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sbs.web.dao.interfaces.OtpDao;
import sbs.web.util.HibernateDaoSupportCustomized;

@Transactional
@Repository("otpDao")
public class OtpDaoImpl extends HibernateDaoSupportCustomized implements OtpDao {

	@Override
	public void saveOtp(Otp otp) {
		// TODO Auto-generated method stub
		
		getSession().saveOrUpdate(otp);
		
	}

	@Override
	public Otp returnOtp(String name) {
		// TODO Auto-generated method stub
		
		Criteria crit = getSession().createCriteria(Otp.class);
		crit.add(Restrictions.eq("username", name));
		if(crit.uniqueResult() != null)
			return (Otp) crit.uniqueResult();
		
		return null;
	}


}
