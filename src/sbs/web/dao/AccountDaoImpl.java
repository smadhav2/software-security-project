package sbs.web.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sbs.web.dao.interfaces.AccountDao;
import sbs.web.util.HibernateDaoSupportCustomized;


@Transactional
@Repository("accountDao")
public class AccountDaoImpl extends HibernateDaoSupportCustomized implements AccountDao {

	@Override
	public void save(Account account) {
		// TODO Auto-generated method stub
		getSession().save(account);
		//this is new comment
	}
	@Override
	public void update(Account account) {
		// TODO Auto-generated method stub
		getSession().update(account);
		//this is new comment
	}
	
	@Override
	public boolean exists(String username,String accountType){
		Criteria crit = getSession().createCriteria(Account.class);
		crit.add(Restrictions.eq("accountType", accountType));
		crit.add(Restrictions.eq("username", username));
		return crit.list().size() >= 1;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Account> findAccountNoForUser(String username) {
		Criteria crit = getSession().createCriteria(Account.class);
		crit.add(Restrictions.eq("username", username));
		if(crit.list()!=null){
			return (List<Account>) crit.list();
		}
		return null;
	}

	@Override
	public Account findAccountforAccno(int accountNumber) {
		return (Account) getHibernateTemplate().find("from Account where AccountNumber = ?", accountNumber).get(0);
		
	}
	@Override
	public Account getAforUCheckings(String username) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(Account.class);
		crit.add(Restrictions.eq("username", username));
		crit.add(Restrictions.eq("accountType","Checkings"));
		if(crit.list()!=null){
			return (Account) crit.uniqueResult();
		}
		return null;
	
	}

}
