package sbs.web.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sbs.web.dao.interfaces.TransactionDao;
import sbs.web.util.HibernateDaoSupportCustomized;

@Transactional
@Repository("transactionDao")
public class TransactionDaoImpl extends HibernateDaoSupportCustomized implements TransactionDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<Transaction> getUserTransaction(int accNo) {
		Criteria crit = getSession().createCriteria(Transaction.class);
		crit.add(Restrictions.eq("accountNumber", accNo));
		
		if(crit.list() !=null){
		return (List<Transaction>) crit.list();
		}
		return null;
	}

	@Transactional
	@Override
	public void makeCredit(Account acc, int i) {
		int amount =  acc.getAccountBalance();
		amount = amount + i;
		acc.setAccountBalance(amount);
		System.out.println("In Credit");
		getSession().update(acc);
		
	}

	@Override
	public void saveTran(Transaction tran) {
		getSession().save(tran);
		
	}
	@Transactional
	@Override
	public void makeDebit(Account acc, int amount) {
		int Am =  acc.getAccountBalance();
		Am = Am - amount;
		System.out.println("IN Debit");
		acc.setAccountBalance(Am);
		getSession().update(acc);
		
	}
	
	public void updateTransaction(Transaction tran){
		
		getHibernateTemplate().update(tran);
	}

	@Transactional
	@Override
	public void makeTransfer(Account acc,Account acc2, Transaction tran, int amount,
			String type) {
	if(type.equals("Debit")){
		int Am =  acc.getAccountBalance();
		Am = Am - amount;
		int Am2 = acc2.getAccountBalance();
		Am2 = Am2 + amount;
		acc.setAccountBalance(Am);
		acc2.setAccountBalance(Am2);
		getSession().update(acc);
		getSession().update(acc2);
	}else
	{
		int Am =  acc.getAccountBalance();
		Am = Am + amount;
		int Am2 = acc2.getAccountBalance();
		Am2 = Am2 - amount;
		acc.setAccountBalance(Am);
		acc2.setAccountBalance(Am2);
		getSession().update(acc);
		getSession().update(acc2);

	}
		
	}
	@SuppressWarnings("unchecked")
	public List<Transaction> penTransactions(){
		Criteria crit = getSession().createCriteria(Transaction.class);
		System.out.println("In");
		crit.add(Restrictions.eq("status", "Pending"));
		
		System.out.println("out");
		
	   
		return (List<Transaction>) crit.list();
	}

	@Override
	public Transaction findByTid(int tid) {
		// TODO Auto-generated method stub
		Criteria crit =  getSession().createCriteria(Transaction.class);
		crit.add(Restrictions.eq("tid", tid));
		return (Transaction) crit.uniqueResult();
	}

	@Override
	public void update(Transaction tran) {
		// TODO Auto-generated method stub
		getHibernateTemplate().update(tran);
	}
	
}
