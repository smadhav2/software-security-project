package sbs.web.dao;

import java.util.List;








import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sbs.web.dao.interfaces.InternalUserRequestDao;
import sbs.web.util.HibernateDaoSupportCustomized;

@Transactional
@Repository("internalUserDao")
public class InternalUserRequestDaoIpml extends HibernateDaoSupportCustomized implements InternalUserRequestDao{

	@Override
	public void save(InternalUserRequest request){
		getHibernateTemplate().save(request);
	}

	@Override
	public void delete(InternalUserRequest request) {
		// TODO Auto-generated method stub
		getHibernateTemplate().delete(request);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InternalUserRequest> getAllInternalUserRequests(String externalUsername) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(InternalUserRequest.class);
		crit.add(Restrictions.eq("externalUsername",externalUsername));
		return crit.list();
	}

	@Override
	public boolean exists(String externalUsername) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(InternalUserRequest.class);
		crit.add(Restrictions.eq("externalUsername",externalUsername));
		return crit.list() !=null;
	}

	@Override
	public InternalUserRequest find(String internalUsername,
			String externalUsername) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(InternalUserRequest.class);
		crit.add(Restrictions.eq("externalUsername",externalUsername));
		crit.add(Restrictions.eq("internalUsername", internalUsername));
		if(crit.uniqueResult() !=null){
			return (InternalUserRequest) crit.uniqueResult();
		}
		return null;
		
	}

	@Override
	public boolean exists(String internalUsername, String externalUsername) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(InternalUserRequest.class);
		crit.add(Restrictions.eq("externalUsername",externalUsername));
		crit.add(Restrictions.eq("internalUsername", internalUsername));
		return crit.uniqueResult() !=null;
	}

	@Override
	public void update(InternalUserRequest request) {
		// TODO Auto-generated method stub
		getHibernateTemplate().update(request);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InternalUserRequest> getListOfApprovedRequests(String internalUsername) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(InternalUserRequest.class);
		crit.add(Restrictions.eq("internalUsername", internalUsername));
	//Criterion crit1 = Restrictions.eq("transactionalReview", 1);
	//Criterion crit2 =  Restrictions.eq("userDetails", 1);
	//	crit.add(Restrictions.or(crit1, crit2));
		return (List<InternalUserRequest>) crit.list();
		
	}
	
}
