package sbs.web.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import sbs.web.customValidationAnnotation.ValidEmail;
import sbs.web.dao.interfaces.FormValidation;
import sbs.web.dao.interfaces.HibernateValidation;

@Entity
@Table(name = "users")
public class User implements Serializable {

	private static final long serialVersionUID = 4806189462386L;

	@GeneratedValue
	@Id
	@Column(name = "userid")
	private int userid;
	
	@Size(min=8, max=50,groups={HibernateValidation.class,FormValidation.class})
	@Column(name = "username")
	@NotEmpty(groups={HibernateValidation.class,FormValidation.class})
	private String username;
	
	@NotEmpty(groups={HibernateValidation.class,FormValidation.class})
	@ValidEmail(min=7, message="This email is not valid",groups={HibernateValidation.class,FormValidation.class})
	@Column(name = "email")
	private String email;
	
	@NotEmpty(groups={HibernateValidation.class,FormValidation.class})
	@Size(min = 8, max= 30, message="Your password does not have a valid size",groups={FormValidation.class})
	@Pattern(regexp="^\\S+$",groups={HibernateValidation.class,FormValidation.class})
	@Column(name = "password")
	private String password;
	
	@Column(name="enabled")
	private boolean enabled=false;
	
	@Column(name="authority")
	private String authority = "ROLE_EXTERNALUSER";
	
	@Column(name="authid")
	private int authid;
	
	@Column(name="accountNonExpired")
	private boolean accountNonExpired= true;

	@Column(name = "publickey")
	private String publicKey;

	@Column(name="accountNonLocked")
	private boolean accountNonLocked = true;
	
	@Column(name="credentialsExpired")
	private boolean credentialsExpired = true;

	public int getAuthid() {
		return authid;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public void setAuthid(int authid) {
		this.authid = authid;
	}

	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public boolean isCredentialsExpired() {
		return credentialsExpired;
	}

	public void setCredentialsExpired(boolean credentialsExpired) {
		this.credentialsExpired = credentialsExpired;
	}

	

	public User() {

	}

	public User(String username, String email, String password,
			boolean enabled, String authority) {

		this.username = username;
		this.password = password;
		this.email = email;
		this.enabled = enabled;
		this.authority = authority;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

}
