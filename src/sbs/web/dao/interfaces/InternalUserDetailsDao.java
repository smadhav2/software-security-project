package sbs.web.dao.interfaces;

import sbs.web.dao.InternalUserDetails;

public interface InternalUserDetailsDao {
	public InternalUserDetails getInternalUserDetails(String username);

	public boolean exists(String username);

	public void save(InternalUserDetails internalUserDetails);
	
	public int getInternalUserId(String username);

	public void update(InternalUserDetails internalUserDetails);


}
