package sbs.web.dao.interfaces;

import java.util.List;

import sbs.web.dao.InternalUserRequest;

public interface InternalUserRequestBo {

	void storeInternalUserRequest(InternalUserRequest request);

	void deleteInternalUserRequest(InternalUserRequest request);

	List<InternalUserRequest> getAllInternalUserRequests(String externalUsername);

	boolean exists(String externalUsername);

}
