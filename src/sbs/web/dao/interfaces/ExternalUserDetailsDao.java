package sbs.web.dao.interfaces;

import sbs.web.dao.ExternalUserDetails;

public interface ExternalUserDetailsDao {

	public ExternalUserDetails getExternalUserDetails(String username);

	public boolean exists(String username);

	public void save(ExternalUserDetails externalUserDetails);
	
	public int getExternalUserId(String username);

	public void update(ExternalUserDetails externalUserDetails);

}
