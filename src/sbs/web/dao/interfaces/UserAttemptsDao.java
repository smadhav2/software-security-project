package sbs.web.dao.interfaces;

import sbs.web.dao.UserAttempts;

public interface UserAttemptsDao {

	public UserAttempts getUserAttempts(String username);
	public void update(UserAttempts userAttempts);
	public void delete(UserAttempts userAttempts);
}
