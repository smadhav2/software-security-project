package sbs.web.dao.interfaces;

import sbs.web.dao.Account;

public interface AccountBo {

	void createAccount(Account account);
	boolean isAccountExists(String username, String accountType);

}
