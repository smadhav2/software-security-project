package sbs.web.dao.interfaces;

import java.util.List;

import sbs.web.dao.InternalUserRequest;

public interface InternalUserRequestDao {

	void save(InternalUserRequest request);

	void delete(InternalUserRequest request);

	List<InternalUserRequest> getAllInternalUserRequests(String externalUsername);

	boolean exists(String externalUsername);

	InternalUserRequest find(String internalUsername, String externalUsername);

	boolean exists(String internalUsername, String externalUsername);

	void update(InternalUserRequest request);

	

	List<InternalUserRequest> getListOfApprovedRequests(
			String internalUserRequest);

}
