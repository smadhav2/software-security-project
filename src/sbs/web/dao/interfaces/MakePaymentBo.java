package sbs.web.dao.interfaces;

import java.util.List;

import sbs.web.dao.MakePayment;
import sbs.web.dao.User;

public interface MakePaymentBo {

	void submitPaymentToUser(MakePayment makepayment);
	public List<MakePayment> paymentApproval(String name);
	public void updateApproved(MakePayment makePayment);
	public List<User> getExternalUser();
	MakePayment findByPaymentId(int paymentid); 
}
