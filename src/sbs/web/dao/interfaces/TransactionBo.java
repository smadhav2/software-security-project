package sbs.web.dao.interfaces;

import java.util.List;

import sbs.web.dao.Account;
import sbs.web.dao.Transaction;

public interface TransactionBo {

	List<Transaction> getTforA(int accNo);

	void doDebit(Account acc, int i);

	void createTran(Transaction tran);

	void doCredit(Account acc, int amount);

	void fundTransfer(Account acc, Account acc2, Transaction tran, int amount,
			String type);

}
