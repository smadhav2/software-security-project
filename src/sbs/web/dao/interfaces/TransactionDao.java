package sbs.web.dao.interfaces;

import java.util.List;

import sbs.web.dao.Account;
import sbs.web.dao.Transaction;

public interface TransactionDao {

	List<Transaction> getUserTransaction(int accNo);

	void makeCredit(Account acc, int i);
	
	public void saveTran(Transaction tran);

	public void makeDebit(Account acc, int amount);

	public void makeTransfer(Account acc, Account acc2, Transaction tran,
			int amount, String type);

	List<Transaction> penTransactions();

	Transaction findByTid(int tid);

	void update(Transaction tran);

}
