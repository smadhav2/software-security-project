package sbs.web.dao.interfaces;



import java.util.List;

import sbs.web.dao.User;

public interface UsersDao {
		User findByUserId(int userid);
		public void createUser(User user);
		public User findUserbyUsername(String Username);
		public boolean exists(String username);
		public List<User> disabledUsers();
		public List<User> externalUsers();
		public void update(User user);
		public boolean findEnabledStatus(String username);
		public boolean findLockedAccount(String username);
		public void deleteUser(User user);
		List<User> getAllExternalUsers();
		List<User> lockedUsers();
		User getKey(String username);
		String getPublicKey(String uname);
}
