package sbs.web.dao.interfaces;

import java.util.List;

import sbs.web.dao.MakePayment;

public interface MakePaymentDao {
	public void save(MakePayment makepayment);
	public List<MakePayment> paymentApproval(String name);
	public void update(MakePayment makePayment);
	public MakePayment findByPaymentid(int paymentid);
	public List<MakePayment> paymentApprovedByUser(String merchantusername);
	public List<MakePayment> paymentRejectedByUser(String merchantusername);
	public List<MakePayment> submitPaymentToBank(String username);
	public List<MakePayment> payemntRejectedByBank(String username);
	public void setEncyptedmessage(MakePayment makePayment);
	public String getMessage(int i);
	public String getEncMessage(int i);
	public MakePayment getMakePayment(int paymentid);
	public List<MakePayment> paymentFromMerchant();
}
