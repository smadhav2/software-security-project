package sbs.web.dao.interfaces;

public interface UserAttemptsBo {
	public void updateFailAttempts(String username);
	public void resetFailAttempts(String username);
}
