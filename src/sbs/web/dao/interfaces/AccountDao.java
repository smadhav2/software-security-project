package sbs.web.dao.interfaces;

import java.util.List;

import sbs.web.dao.Account;

public interface AccountDao {

	public void save(Account account);
	boolean exists(String username, String accountType);
	public List<Account> findAccountNoForUser(String name);
	public Account findAccountforAccno(int accountNumber);
	public void update(Account account);
	public Account getAforUCheckings(String username);

}
