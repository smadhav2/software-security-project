package sbs.web.dao.interfaces;

import sbs.web.dao.PaymentToBank;

public interface PaymentToBankDao {

	public void save(PaymentToBank paymentToBank);

	public PaymentToBank getEncryptedMessage(int paymentid, String username);

	public void delete(PaymentToBank pk1);

}
