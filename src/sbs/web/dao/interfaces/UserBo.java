package sbs.web.dao.interfaces;


import sbs.web.dao.User;


public interface UserBo {
	public void save(User user);
	public User findByUserName(String username);
	public boolean exists(String username);
}
