package sbs.web.dao.interfaces;

import sbs.web.dao.PaymentToBank;

public interface PaymentToBankBo {

	public void savePayment(PaymentToBank paymentToBank);

}
