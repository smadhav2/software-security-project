package sbs.web.dao.interfaces;

import sbs.web.dao.Otp;

public interface OtpDao {

	public Otp returnOtp(String name);
	void saveOtp(Otp otp);
}
