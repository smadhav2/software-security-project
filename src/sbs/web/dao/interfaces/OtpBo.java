package sbs.web.dao.interfaces;

import sbs.web.dao.Otp;

public interface OtpBo {

	void saveToOtp(Otp otp);

	Otp returnOtp(String name);
}
