package sbs.web.dao.interfaces;

import java.util.List;

import sbs.web.dao.User;

public interface AdminBo {

	public List<User> getDisabledUser();
	public void enableUser(User user);
}
