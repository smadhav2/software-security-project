package sbs.web.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sbs.web.dao.interfaces.MakePaymentDao;
import sbs.web.util.HibernateDaoSupportCustomized;

@Transactional
@Repository("makePaymentDao")
public class MakePaymentDaoImpl extends HibernateDaoSupportCustomized implements MakePaymentDao {

	@Override
	public void save(MakePayment makepayment) {
		// TODO Auto-generated method stub
		getSession().save(makepayment);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MakePayment> paymentApproval(String name) {
		Criteria crit = getSession().createCriteria(MakePayment.class);
		crit.add(Restrictions.eq("username", name));
		crit.add(Restrictions.eq("approved", 0));
		System.out.println(name);
//		crit.setProjection(Projections.projectionList().add(Projections.property("paymentid"), "paymentid")
//				.add(Projections.property("merchantusername"), "merchantusername")
//				.add(Projections.property("username"), "username")
//				.add(Projections.property("amount"), "amount")
//				.add(Projections.property("quantity"), "quantity")
//				.add(Projections.property("totalAmount"), "totalAmount")
//				.add(Projections.property("description"), "description")
//				.add(Projections.property("approved"), "approved"));
		
		//crit.setResultTransformer(new AliasToBeanResultTransformer(MakePayment.class));
		return (List<MakePayment>) crit.list();
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(MakePayment makePayment) {
		// TODO Auto-generated method stub
		getHibernateTemplate().update(makePayment);
	}
	
	@Override
	public MakePayment findByPaymentid(int paymentid) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(MakePayment.class);
		crit.add(Restrictions.eq("paymentid", paymentid));
		return (MakePayment) crit.uniqueResult();
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MakePayment> paymentApprovedByUser(String merchantusername) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(MakePayment.class);
		crit.add(Restrictions.eq("merchantusername", merchantusername));
		crit.add(Restrictions.eq("approved", 1));
//		crit.setProjection(Projections.projectionList().add(Projections.property("paymentid"), "paymentid")
//				.add(Projections.property("merchantusername"), "merchantusername")
//				.add(Projections.property("username"), "username")
//				.add(Projections.property("amount"), "amount")
//				.add(Projections.property("quantity"), "quantity")
//				.add(Projections.property("totalAmount"), "totalAmount")
//				.add(Projections.property("description"), "description")
//				.add(Projections.property("approved"), "approved"));
//		
//		crit.setResultTransformer(new AliasToBeanResultTransformer(MakePayment.class));
		return (List<MakePayment>) crit.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MakePayment> paymentRejectedByUser(String merchantusername) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(MakePayment.class);
		crit.add(Restrictions.eq("merchantusername", merchantusername));
		crit.add(Restrictions.eq("approved", 2));
//		crit.setProjection(Projections.projectionList().add(Projections.property("paymentid"), "paymentid")
//				.add(Projections.property("merchantusername"), "merchantusername")
//				.add(Projections.property("username"), "username")
//				.add(Projections.property("amount"), "amount")
//				.add(Projections.property("quantity"), "quantity")
//				.add(Projections.property("totalAmount"), "totalAmount")
//				.add(Projections.property("description"), "description")
//				.add(Projections.property("approved"), "approved"));
//		
//		crit.setResultTransformer(new AliasToBeanResultTransformer(MakePayment.class));
		return (List<MakePayment>) crit.list();
	}

	@Override
	public List<MakePayment> submitPaymentToBank(String merchantusername) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(MakePayment.class);
		crit.add(Restrictions.eq("merchantusername", merchantusername));
		crit.add(Restrictions.eq("approved", 3));
//		crit.setProjection(Projections.projectionList().add(Projections.property("paymentid"), "paymentid")
//				.add(Projections.property("merchantusername"), "merchantusername")
//				.add(Projections.property("username"), "username")
//				.add(Projections.property("amount"), "amount")
//				.add(Projections.property("quantity"), "quantity")
//				.add(Projections.property("totalAmount"), "totalAmount")
//				.add(Projections.property("description"), "description")
//				.add(Projections.property("approved"), "approved"));
//		
//		crit.setResultTransformer(new AliasToBeanResultTransformer(MakePayment.class));
		return (List<MakePayment>) crit.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MakePayment> payemntRejectedByBank(String merchantusername) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(MakePayment.class);
		crit.add(Restrictions.eq("merchantusername", merchantusername));
		crit.add(Restrictions.eq("approved", 4));
//		crit.setProjection(Projections.projectionList().add(Projections.property("paymentid"), "paymentid")
//				.add(Projections.property("merchantusername"), "merchantusername")
//				.add(Projections.property("username"), "username")
//				.add(Projections.property("amount"), "amount")
//				.add(Projections.property("quantity"), "quantity")
//				.add(Projections.property("totalAmount"), "totalAmount")
//				.add(Projections.property("description"), "description")
//				.add(Projections.property("approved"), "approved"));
//		
//		crit.setResultTransformer(new AliasToBeanResultTransformer(MakePayment.class));
		return (List<MakePayment>) crit.list();
	}

	@Override
	public void setEncyptedmessage(MakePayment makePayment) {
		// TODO Auto-generated method stub
		
		getHibernateTemplate().update(makePayment);
		
	}

	@Override
	public String getMessage(int i) {
		// TODO Auto-generated method stub
		
		Criteria crit = getSession().createCriteria(MakePayment.class);
		crit.add(Restrictions.eq("paymentid", i));
		
		return ((MakePayment)crit.uniqueResult()).getMessage();
	}

	@Override
	public String getEncMessage(int i) {
		// TODO Auto-generated method stub
		
		Criteria crit = getSession().createCriteria(MakePayment.class);
		crit.add(Restrictions.eq("paymentid", i));
		return ((MakePayment)crit.uniqueResult()).getEncmessage();
	}

	@Override
	public MakePayment getMakePayment(int paymentid) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(MakePayment.class);
		crit.add(Restrictions.eq("paymentid", paymentid));
		return (MakePayment) crit.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MakePayment> paymentFromMerchant() {
		Criteria crit = getSession().createCriteria(MakePayment.class);
		crit.add(Restrictions.eq("approved", 3));
		crit.setProjection(Projections.projectionList().add(Projections.property("paymentid"), "paymentid")
				.add(Projections.property("merchantusername"), "merchantusername")
				.add(Projections.property("username"), "username")
				.add(Projections.property("amount"), "amount")
				.add(Projections.property("quantity"), "quantity")
				.add(Projections.property("totalAmount"), "totalAmount")
				.add(Projections.property("description"), "description")
				.add(Projections.property("approved"), "approved"));
		
		crit.setResultTransformer(new AliasToBeanResultTransformer(MakePayment.class));
		return (List<MakePayment>) crit.list();
	}

}
