package sbs.web.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sbs.web.dao.interfaces.ExternalUserDetailsDao;
import sbs.web.util.HibernateDaoSupportCustomized;

@Transactional
@Repository("externalUserDetailsDao")
public class ExternalUserDetailsDaoImpl extends HibernateDaoSupportCustomized implements ExternalUserDetailsDao {

	@Override
	public ExternalUserDetails getExternalUserDetails(String username){
		Criteria crit = getSession().createCriteria(ExternalUserDetails.class);
		crit.add(Restrictions.eq("username",username));
		if(crit.uniqueResult()!=null){
			return (ExternalUserDetails) crit.uniqueResult();
		}
		return null;
	}

	@Override
	public boolean exists(String username) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(ExternalUserDetails.class);
		crit.add(Restrictions.eq("username",username));
		if(crit.uniqueResult()!=null)
			return true;
		else
			return false;
	}
	
	@Override
	public void save(ExternalUserDetails externalUserDetails){
		getHibernateTemplate().save(externalUserDetails);
	}
	

	@Override
	public int getExternalUserId(String username) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(ExternalUserDetails.class);
		crit.add(Restrictions.eq("username",username));
		if(crit.uniqueResult()!=null)
			return ((ExternalUserDetails)crit.uniqueResult()).getExternaluserdetialid();
		
		return 0;
	}

	@Override
	public void update(ExternalUserDetails externalUserDetails) {
		// TODO Auto-generated method stub
		getHibernateTemplate().update(externalUserDetails);
	}
	
}
