package sbs.web.dao;


import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sbs.web.dao.interfaces.UserAttemptsDao;
import sbs.web.util.HibernateDaoSupportCustomized;

@Transactional
@Repository("userDetailsDao")
public class UserAttemptsDaoImpl extends HibernateDaoSupportCustomized implements UserAttemptsDao{

	
	
	
	@Override
	public void update(UserAttempts userAttempts) {
		// TODO Auto-generated method stub
		getHibernateTemplate().saveOrUpdate(userAttempts);
	}

	
	@Override
	public UserAttempts getUserAttempts(String username) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(UserAttempts.class);
		crit.add(Restrictions.eq("username",username));
		if(crit.uniqueResult()!=null){
		return (UserAttempts) crit.uniqueResult();
		}
		return null;
	}


	@Override
	public void delete(UserAttempts userAttempts) {
		// TODO Auto-generated method stub
		getHibernateTemplate().delete(userAttempts);;
	}
	
	

}
