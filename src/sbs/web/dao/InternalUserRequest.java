package sbs.web.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="admin_requests")
public class InternalUserRequest {
	
	@Id
	@Column(name="requestid")
	private int requestid;
	
	@Column(name="admin_username")
	private String internalUsername;
	
	@Column(name="requested_username")
	private String externalUsername;
	
	@Column(name="transactional_review")
	private int transactionalReview = -1;
	
	@Column(name="user_details")
	private int userDetails = -1;
	
	@Column(name="account_details")
	private int accountDetails = -1;
	
	public int getTransactionalReview() {
		return transactionalReview;
	}

	public void setTransactionalReview(int transactionalReview) {
		this.transactionalReview = transactionalReview;
	}

	public int getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(int userDetails) {
		this.userDetails = userDetails;
	}

	public int getAccountDetails() {
		return accountDetails;
	}

	public void setAccountDetails(int accountDetails) {
		this.accountDetails = accountDetails;
	}

	public String getInternalUsername() {
		return internalUsername;
	}

	public void setInternalUsername(String internalUsername) {
		this.internalUsername = internalUsername;
	}

	public String getExternalUsername() {
		return externalUsername;
	}

	public void setExternalUsername(String externalUsername) {
		this.externalUsername = externalUsername;
	}
	

	
	
}
