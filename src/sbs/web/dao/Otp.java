package sbs.web.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "otp")
public class Otp {

	@Id
	@Column(name = "username", nullable = false, length = 100)
	private String username;

	@Column(name = "otpTimeStamp", nullable = false, length = 100)
	private long otpTimeStamp;

	@Column(name = "otpcode", nullable = false, length = 8)
	private String otpCode;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public long getOtpTimeStamp() {
		return otpTimeStamp;
	}

	public void setOtpTimeStamp(long otpTimeStamp) {
		this.otpTimeStamp = otpTimeStamp;
	}

	public String getOtpCode() {
		return otpCode;
	}

	public void setOtpCode(String otpCode) {
		this.otpCode = otpCode;
	}

}
