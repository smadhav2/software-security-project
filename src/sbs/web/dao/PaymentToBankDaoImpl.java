package sbs.web.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sbs.web.dao.interfaces.PaymentToBankDao;
import sbs.web.util.HibernateDaoSupportCustomized;

@Transactional
@Repository("paymentToBankDao")
public class PaymentToBankDaoImpl extends HibernateDaoSupportCustomized implements PaymentToBankDao{

	@Override
	public void save(PaymentToBank paymentToBank) {
		// TODO Auto-generated method stub
		getSession().save(paymentToBank);
	}

	@Override
	public PaymentToBank getEncryptedMessage(int paymentid, String username) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(PaymentToBank.class);
		crit.add(Restrictions.eq("paymentid", paymentid));
		crit.add(Restrictions.eq("username", username));
		return ((PaymentToBank)crit.uniqueResult());
		
		
	}

	@Override
	public void delete(PaymentToBank pk1) {
		// TODO Auto-generated method stub
		getHibernateTemplate().delete(pk1);
	}
	

}
