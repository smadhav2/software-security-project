package sbs.web.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sbs.web.dao.interfaces.UsersDao;
import sbs.web.util.HibernateDaoSupportCustomized;

@Transactional
@Repository("usersDao")
public class UsersDaoImpl extends HibernateDaoSupportCustomized implements UsersDao{
	@Autowired
	private PasswordEncoder passwordencoder;
	
	@Transactional
	public void createUser(User user){
		user.setPassword(passwordencoder.encode(user.getPassword()));
		//user.setCertificate(certificate);
		getSession().save(user);
	}
	@SuppressWarnings("unchecked")
	public List<User> getAllUsers(){
		return getHibernateTemplate().find("from User");
	}
	
	public void deleteUser(User user){
		getSession().delete(user);
	}
	public User findByUserId(int userid) {
		// TODO Auto-generated method stub
		return (User) getHibernateTemplate().find("from User  where =?", userid).get(0);
	}
	public User findUserbyUsername(String Username){
		return (User) getHibernateTemplate().find("from User where username = ?", Username).get(0);
	}
	@Override
	public boolean exists(String username) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(User.class);
		crit.add(Restrictions.eq("username", username));
		User user = (User) crit.uniqueResult();
		return user != null ;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> disabledUsers(){
		Criteria crit = getSession().createCriteria(User.class);
		crit.add(Restrictions.eq("enabled", false));
		crit.setProjection(Projections.projectionList().add(Projections.property("username"),"username")
		.add(Projections.property("email"),"email")
		.add(Projections.property("authority"),"authority"));
		
		crit.setResultTransformer(new AliasToBeanResultTransformer(User.class));
		return (List<User>) crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<User> externalUsers(){
		
		Criteria crit = getSession().createCriteria(User.class);
		crit.add(Restrictions.eq("authority", "ROLE_EXTERNALUSER"));
		crit.setProjection(Projections.projectionList().add(Projections.property("username"),"username"));
		crit.setResultTransformer(new AliasToBeanResultTransformer(User.class));
		return (List<User>) crit.list();
		
		
	}
	@Override
	public void update(User user) {
		// TODO Auto-generated method stub
		user.setPassword(passwordencoder.encode(user.getPassword()));
		getHibernateTemplate().update(user);
	}
	
	@Override
	public boolean findEnabledStatus(String username) {
		// TODO Auto-generated method stub
		boolean status = false;
		Criteria crit = getSession().createCriteria(User.class);
		crit.add(Restrictions.eq("username", username));
		if(crit.uniqueResult() != null){
		 status = ((User)crit.uniqueResult()).isEnabled();
		 }
		return status;
	}
	@Override
	public boolean findLockedAccount(String username) {
		// TODO Auto-generated method stub
		boolean status = false;
		Criteria crit = getSession().createCriteria(User.class);
		crit.add(Restrictions.eq("username", username));
		if(crit.uniqueResult() != null){
		 status = ((User)crit.uniqueResult()).isAccountNonLocked();
		 }
		return status;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAllExternalUsers() {
		// TODO Auto-generated method stub
		
		Criteria crit = getSession().createCriteria(User.class);
		crit.add(Restrictions.eq("authority", "ROLE_EXTERNALUSER"));
		crit.setProjection(Projections.projectionList().add(Projections.property("username"),"username"));
		
		crit.setResultTransformer(new AliasToBeanResultTransformer(User.class));
		return (List<User>) crit.list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<User> lockedUsers() {
		// TODO Auto-generated method stub


		Criteria crit = getSession().createCriteria(User.class);
		crit.add(Restrictions.eq("accountNonLocked", false));
		crit.setProjection(Projections.projectionList().add(Projections.property("username"),"username")
		.add(Projections.property("email"),"email")
		.add(Projections.property("authority"),"authority"));
		
		crit.setResultTransformer(new AliasToBeanResultTransformer(User.class));
		return (List<User>) crit.list();
	}
	@Override
	public User getKey(String username) {
		Criteria crit = getSession().createCriteria(User.class);
		crit.add(Restrictions.eq("username", username));
		//crit.setProjection(Projections.projectionList().add(Projections.property("publickey"),"publickey"));
		
		//crit.setResultTransformer(new AliasToBeanResultTransformer(User.class));
		
		User user= (User)crit.uniqueResult();
		
		return user;
		// TODO Auto-generated method stub
		
	}
	@Override
	public String getPublicKey(String uname) {
		// TODO Auto-generated method stub
		Criteria crit = getSession().createCriteria(User.class);
		crit.add(Restrictions.eq("username", uname));
		User user = (User)crit.uniqueResult();
		return user.getPublicKey();
	}
}
