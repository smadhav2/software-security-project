package sbs.web.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name="paymenttobank")
public class PaymentToBank {

	@GeneratedValue
	@Id
	@Column(name="serial")
	private int serial;
	
	@Column(name="username")
	private String username;
	
	@Column(name="paymentid")
	private int paymentid;
	
	@Column(name="encryptedmessage")
	private String encryptedmessage;
	
	@Column(name="message")
	private String message;
	
	@Column(name="authority")
	private String authority;

	public int getSerial() {
		return serial;
	}

	public void setSerial(int serial) {
		this.serial = serial;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getPaymentid() {
		return paymentid;
	}

	public void setPaymentid(int paymentid) {
		this.paymentid = paymentid;
	}

	public String getEncryptedmessage() {
		return encryptedmessage;
	}

	public void setEncryptedmessage(String encryptedmessage) {
		this.encryptedmessage = encryptedmessage;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	@Override
	public String toString() {
		return "PaymentToBank [serial=" + serial + ", username=" + username
				+ ", paymentid=" + paymentid + ", encryptedmessage="
				+ encryptedmessage + ", message=" + message + ", authority="
				+ authority + "]";
	}

	public PaymentToBank(int serial, String username, int paymentid,
			String encryptedmessage, String message, String authority) {
		super();
		this.serial = serial;
		this.username = username;
		this.paymentid = paymentid;
		this.encryptedmessage = encryptedmessage;
		this.message = message;
		this.authority = authority;
	}
	
	public PaymentToBank() {
		
	}
}
