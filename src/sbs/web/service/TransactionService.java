package sbs.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sbs.web.dao.Account;
import sbs.web.dao.Transaction;
import sbs.web.dao.interfaces.AccountDao;
import sbs.web.dao.interfaces.TransactionBo;
import sbs.web.dao.interfaces.TransactionDao;

@Service("transactionBo")
public class TransactionService implements TransactionBo {

	@Autowired
	private TransactionDao transactionDao;
	
	@Autowired
	private AccountDao accountDao;
	
	@Override
	public List<Transaction> getTforA(int accNo) {
		
		return transactionDao.getUserTransaction(accNo);
	}

	@Override
	public void doDebit(Account acc, int amount) {
		int Am =  acc.getAccountBalance();
		Am = Am - amount;
		
		if(Am > 0 && amount>0){
		acc.setAccountBalance(Am);
		acc.setAvailableBalance(Am);
		System.out.println(acc.getAvailableBalance());
		accountDao.update(acc);
		}
		else{
			throw new NumberFormatException("Your balance is not sufficient");
		}
		
		
	}
	
	@Override
	public void createTran(Transaction tran) {
		transactionDao.saveTran(tran);
		
	}
	@Override
	public void doCredit(Account acc, int amount) {
		int Am =  acc.getAccountBalance();
		Am = Am + amount;
		if(Am > 0 && amount > 0 ){
		acc.setAccountBalance(Am);
		acc.setAvailableBalance(Am);
		accountDao.update(acc);
		}
		else{
			throw new NumberFormatException("Your balance is not sufficient");
		}
		
	}
	@Override
	public void fundTransfer(Account acc, Account acc2, Transaction tran, int amount,
			String type) {
		
			doDebit(acc, amount);
			doCredit(acc2, amount);
			
				
		
	}

	public Transaction findByTid(int tid) {
		// TODO Auto-generated method stub
		
		return transactionDao.findByTid(tid);
	}

	public void update(Transaction tran) {
		// TODO Auto-generated method stub
		transactionDao.update(tran);
	}
}
