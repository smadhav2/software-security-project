package sbs.web.service;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.stereotype.Service;
import sbs.web.dao.interfaces.LoginAuthenticationBo;

@Service("loginAuthenticationBo")
public class LoginAuthenticationService extends JdbcDaoImpl implements LoginAuthenticationBo{
	
	@Autowired
	private DataSource dataSource;
	
	@PostConstruct
	private void initialize(){
		setDataSource(dataSource);
	}

@Override
@Value("select username,authority from users where binary username=?")
	public void setAuthoritiesByUsernameQuery(String queryString) {
		
		super.setAuthoritiesByUsernameQuery(queryString);
	}
	
	@Override
	@Value("select username,password,enabled from users where binary username = ?")
	public void setUsersByUsernameQuery(String usersByUsernameQueryString) {
		
		super.setUsersByUsernameQuery(usersByUsernameQueryString);
	}
}
