package sbs.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import sbs.web.dao.InternalUserDetails;
import sbs.web.dao.interfaces.InternalUserDetailsBo;
import sbs.web.dao.interfaces.InternalUserDetailsDao;


@Service("internalUserDetailsBo")
public class InternalUserDetailsService implements InternalUserDetailsBo {

	@Autowired
	private InternalUserDetailsDao internalUserDetailsDao;
	
	public InternalUserDetails getInternalUserDetails(String username){
		
		
		return internalUserDetailsDao.getInternalUserDetails(username);
	}
	
	public boolean internalUserDetailsExist(String username){
	if(internalUserDetailsDao.exists(username)){
		return true;
		
	}
		return false;
	}
	public void saveInternalUserDetails(InternalUserDetails internalUserDetails){
		internalUserDetailsDao.save(internalUserDetails);
	}

	public int getInternalUserid(String username) {
		// TODO Auto-generated method stub
		return internalUserDetailsDao.getInternalUserId(username);
		
	}

	public void updateInternalUserDetails(
			InternalUserDetails internalUserDetails) {
		// TODO Auto-generated method stub
		internalUserDetailsDao.update(internalUserDetails);
	}
}
