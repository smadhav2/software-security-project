package sbs.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import sbs.web.dao.Account;
import sbs.web.dao.interfaces.AccountBo;
import sbs.web.dao.interfaces.AccountDao;

@Service("accountBo")
public class AccountService implements AccountBo {

	@Autowired
	private AccountDao accountDao;

	@Override
	public void createAccount(Account account) {
		// TODO Auto-generated method stub		
		accountDao.save(account);
	}
	
	public void updateAccount(Account account){
		accountDao.update(account);
	}
	
	@Override
	public boolean isAccountExists(String username,String accountType) {
		
		if(accountDao.exists(username,accountType)){
			throw new DuplicateKeyException("This user already has an account of type: " + accountType); 
			
		}
			return false;
	}
	public List<Account> getAforU(String name){
		
		return accountDao.findAccountNoForUser(name);
	}

	public Account getAforA(int accountNumber) {
		return accountDao.findAccountforAccno(accountNumber);
	}

	public Account getAforUCheckings(String username) {
		// TODO Auto-generated method stub
		
		return accountDao.getAforUCheckings(username);
	}
	
}
