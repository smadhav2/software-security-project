package sbs.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sbs.web.dao.User;
import sbs.web.dao.interfaces.AdminBo;
import sbs.web.dao.interfaces.TransactionDao;
import sbs.web.dao.interfaces.UsersDao;


@Service("adminBo")
public class AdminService implements AdminBo {
	
	@Autowired
	private UsersDao userDao;
	@Autowired
	private TransactionDao tranDao;
	public List<User> getDisabledUser() {
		return userDao.disabledUsers();
	}
	public void enableUser(User user){
		userDao.update(user);
	}
	public void delete(User user) {
		// TODO Auto-generated method stub
		userDao.deleteUser(user);
	}
	public Object getPendingTransactions() {
		tranDao.penTransactions();
		return null;
	}
	public List<User> getLockedUser() {
		// TODO Auto-generated method stub
		return userDao.lockedUsers();
	}

}
