package sbs.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;

import sbs.web.dao.User;
import sbs.web.dao.UserAttempts;
import sbs.web.dao.interfaces.UserAttemptsBo;
import sbs.web.dao.interfaces.UserAttemptsDao;
import sbs.web.dao.interfaces.UsersDao;

@Service("userAttemptsBo")
public class UserAttemptsService implements UserAttemptsBo{

	@Autowired
	private UserAttemptsDao userAttemptsDao;
	
	@Autowired
	private UsersDao usersDao;
	
	private static final int MAX_ATTEMPTS = 3;
	
	@Override
	public void updateFailAttempts(String username) {
		UserAttempts userAttempts = userAttemptsDao.getUserAttempts(username);
		if(!usersDao.exists(username)){
			throw new BadCredentialsException("Username does not exist");
		}
		if(userAttempts == null){
			userAttempts = new UserAttempts();
			userAttempts.setUsername(username);
		}
		if(userAttempts.getAttempts()< MAX_ATTEMPTS){
		userAttempts.setAttempts(userAttempts.getAttempts() + 1);
		userAttemptsDao.update(userAttempts);
		}
	}

	@Override
	public void resetFailAttempts(String username) {
		// TODO Auto-generated method stub
		UserAttempts userAttempts = userAttemptsDao.getUserAttempts(username);
		userAttemptsDao.delete(userAttempts);
	}

	public UserAttempts getUserAttempts(String username) {
		// TODO Auto-generated method stub
		UserAttempts userAttempts = userAttemptsDao.getUserAttempts(username);
		return userAttempts;
	}

	public void lockUserAccount(String username){

		User user = usersDao.findUserbyUsername(username);
		user.setAccountNonLocked(false);
		usersDao.update(user);
	}
	public int getNumberOfUserAttempts(String username){
		UserAttempts userattempts = userAttemptsDao.getUserAttempts(username);
		if(userattempts!=null){
			return userattempts.getAttempts();
			
		}
		return -1;
	}
	
	public boolean getUserAccountStatus(String username){
		return usersDao.findEnabledStatus(username);
		
	}
	
	public boolean getUserAccountLockStatus(String username){
		return usersDao.findLockedAccount(username);
	}
	
}
