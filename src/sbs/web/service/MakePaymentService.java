package sbs.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sbs.web.dao.MakePayment;
import sbs.web.dao.User;
import sbs.web.dao.interfaces.MakePaymentBo;
import sbs.web.dao.interfaces.MakePaymentDao;
import sbs.web.dao.interfaces.UsersDao;

@Service("makePaymentBo")
public class MakePaymentService implements MakePaymentBo   {
	
	@Autowired
	private MakePaymentDao makePaymentDao;
	
    @Autowired
	private UsersDao userDao;

	public Object makePayment;
    
	@Override
	public void submitPaymentToUser(MakePayment makepayment) {
		// TODO Auto-generated method stub
		makePaymentDao.save(makepayment);
	}
	
	public void updateApproved(MakePayment makePayment) {
		makePaymentDao.update(makePayment);
	}
	public List<MakePayment> paymentApproval(String name) {
		return makePaymentDao.paymentApproval(name);
	}
	
	
	public List<User> getExternalUser() {
		return userDao.externalUsers();
	}
	
	@Override
	public MakePayment findByPaymentId(int paymentid) {
		// TODO Auto-generated method stub
		return  makePaymentDao.findByPaymentid(paymentid);
	}

	public List<MakePayment> paymentApprovedByUser(String merchantusername) {
		return makePaymentDao.paymentApprovedByUser(merchantusername);
	}

	public List<MakePayment> paymentRejectedByUser(String merchantusername) {
		// TODO Auto-generated method stub
		return makePaymentDao.paymentRejectedByUser(merchantusername);
	}

	public List<MakePayment> submitPaymentToBank(String username) {
		// TODO Auto-generated method stub
		return makePaymentDao.submitPaymentToBank(username);
	}

	public List<MakePayment> payemntRejectedByBank(String username) {
		// TODO Auto-generated method stub
		return makePaymentDao.payemntRejectedByBank(username);
	}

	public void setEncyptedmessage(MakePayment makePayment) {
	 makePaymentDao.setEncyptedmessage(makePayment);
		
	}

	public String getMessage(int i) {
		// TODO Auto-generated method stub
		return makePaymentDao.getMessage(i);
	}

	public String getEncMessage(int i) {
		// TODO Auto-generated method stub
		return makePaymentDao.getEncMessage(i);
		
	}

	public MakePayment getMakePayment(int paymentid) {
		// TODO Auto-generated method stub
		return makePaymentDao.getMakePayment(paymentid);
	}

	public void update(MakePayment mk) {
		// TODO Auto-generated method stub
		makePaymentDao.update(mk);
	}
	
	public List<MakePayment> paymentFromMerchant() {
		// TODO Auto-generated method stub
		return makePaymentDao.paymentFromMerchant();
	}
}

