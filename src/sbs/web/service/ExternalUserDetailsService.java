package sbs.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sbs.web.dao.ExternalUserDetails;
import sbs.web.dao.interfaces.ExternalUserDetailsBo;
import sbs.web.dao.interfaces.ExternalUserDetailsDao;

@Service("externalUserDetailsBo")
public class ExternalUserDetailsService implements ExternalUserDetailsBo {

	@Autowired
	private ExternalUserDetailsDao externalUserDetailsDao;
	
	public ExternalUserDetails getExternalUserDetails(String username){
		
		
		return externalUserDetailsDao.getExternalUserDetails(username);
	}
	
	public boolean externalUserDetailsExist(String username){
	if(externalUserDetailsDao.exists(username)){
		return true;
		
	}
		return false;
	}
	public void saveExternalUserDetails(ExternalUserDetails externalUserDetails){
		externalUserDetailsDao.save(externalUserDetails);
	}

	public int getExternalUserid(String username) {
		// TODO Auto-generated method stub
		return externalUserDetailsDao.getExternalUserId(username);
		
	}

	public void updateExternalUserDetails(
			ExternalUserDetails externalUserDetails) {
		// TODO Auto-generated method stub
		externalUserDetailsDao.update(externalUserDetails);
	}
}
