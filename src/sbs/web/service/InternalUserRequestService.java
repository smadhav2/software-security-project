package sbs.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sbs.web.dao.InternalUserRequest;
import sbs.web.dao.interfaces.InternalUserRequestBo;
import sbs.web.dao.interfaces.InternalUserRequestDao;

@Service("internalUserRequestBo")
public class InternalUserRequestService implements InternalUserRequestBo {

	@Autowired
	private InternalUserRequestDao internalUserRequestDao;
	
	@Override
	public void storeInternalUserRequest(InternalUserRequest request){
		internalUserRequestDao.save(request);
	}
	@Override
	public void deleteInternalUserRequest(InternalUserRequest request){
		internalUserRequestDao.delete(request);
	}
	@Override
	public List<InternalUserRequest> getAllInternalUserRequests(String internalUsername){
		
		return internalUserRequestDao.getAllInternalUserRequests(internalUsername);
	}
	
	@Override
	public boolean exists(String externalUsername){
		return internalUserRequestDao.exists(externalUsername);
	}
	public InternalUserRequest find(String internalUsername, String externalUsername) {
		// TODO Auto-generated method stub
		
		return internalUserRequestDao.find(internalUsername, externalUsername);
	}
	public boolean exists(String internalUsername, String externalUsername) {
		// TODO Auto-generated method stub
		
		return internalUserRequestDao.exists(internalUsername, externalUsername);
	}
	public void update(InternalUserRequest request) {
		// TODO Auto-generated method stub
		 internalUserRequestDao.update(request);
	}
	public List<InternalUserRequest> getListOfApprovedRequests(String username){
		
		return internalUserRequestDao.getListOfApprovedRequests(username);
		
	}
	}
