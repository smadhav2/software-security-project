package sbs.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sbs.web.dao.PaymentToBank;
import sbs.web.dao.interfaces.PaymentToBankBo;
import sbs.web.dao.interfaces.PaymentToBankDao;

@Service("paymentToBankBo")
public class PaymentToBankService implements PaymentToBankBo{

	@Autowired
	private PaymentToBankDao paymentToBankDao;
	
	@Override
	public void savePayment(PaymentToBank paymentToBank) {
		// TODO Auto-generated method stub
		paymentToBankDao.save(paymentToBank);
	}

	public PaymentToBank getEncryptedMessage(int paymentid, String username) {
		// TODO Auto-generated method stub
		
		return paymentToBankDao.getEncryptedMessage(paymentid,username);
		
	}

	public void delete(PaymentToBank pk1) {
		// TODO Auto-generated method stub
		paymentToBankDao.delete(pk1);
	}


}
