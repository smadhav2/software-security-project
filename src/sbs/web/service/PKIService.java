package sbs.web.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;

@Service("pkiService")
public class PKIService {

	private static final String ENCRYPTION_ALGO = "RSA";
	private static final int NUMBER_BITS = 1024;
	private static final String SIGNATURE_ALGO = "SHA256withRSA";
	private KeyPair pair;

	public void initializePair() {
		KeyPair keyPair;
		try {
			// KeyPairGenerator initialized
			KeyPairGenerator keygen = KeyPairGenerator.getInstance(
					ENCRYPTION_ALGO);
			keygen.initialize(NUMBER_BITS, new SecureRandom());

			// Generating key pair
			keyPair = keygen.generateKeyPair();
			this.pair = keyPair;

		} catch (NoSuchAlgorithmException | SecurityException e) {
			e.printStackTrace();
		}
	}

	public KeyPair getPair() {
		return pair;
	}

	/**
	 * Send an email containing certificate and private key as attachment to the
	 * user once admin approves the enrollment request.
	 * 
	 * @param certificate
	 *            - User certificate
	 * @param username
	 *            - User id against which the certificate is generated
	 * @param privKey
	 *            - User's private key
	 */
	public File getPrivateKey(String username,
			PrivateKey privKey) {
		try {
			File file = new File("PrivateKey"
					+ username + ".key");
			FileOutputStream fosPrivate = new FileOutputStream(file);
			PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(
					privKey.getEncoded());
			fosPrivate.write(pkcs8EncodedKeySpec.getEncoded());
			fosPrivate.close();
			return file;
		} catch (Exception e) {
			System.out.println("In sendCert module" + e.getMessage());
		}
		return null;
	}
	
	public void deleteFile(String username){
		File file = new File("PrivateKey"
				+ username + ".key");
		if(file!=null)
			file.delete();
	}

	public boolean verifySign(String signedDataStr, String dataStr, byte[] publicKey) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException {
			byte[] signedData = signedDataStr.getBytes();
			
			// Decoding the base64 encoded data
			byte[] signedDataDecoded = Base64.decode(signedData);
			byte[] data = dataStr.getBytes();
			// Decoding the base64 encoded public key
			byte[] decodedPublicKey = Base64.decode(publicKey);
			KeyFactory keyFactory = KeyFactory.getInstance(ENCRYPTION_ALGO);
			X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(decodedPublicKey);
			PublicKey pubKey = keyFactory.generatePublic(publicKeySpec);
			Signature sig = Signature.getInstance(SIGNATURE_ALGO);
			sig.initVerify(pubKey);
			sig.update(data, 0, data.length);
			return sig.verify(signedDataDecoded);
	}

	public String encrypt(byte[] data, byte[] publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
			byte[] decodedPublicKey = Base64.decode(publicKey);
			KeyFactory keyFactory = KeyFactory.getInstance(ENCRYPTION_ALGO);
			X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(decodedPublicKey);
			PublicKey pubKey = keyFactory.generatePublic(publicKeySpec);
			Cipher cipher = Cipher.getInstance(ENCRYPTION_ALGO);
			cipher.init(Cipher.ENCRYPT_MODE, pubKey);
			byte[] encrypted = cipher.doFinal(data);
			byte[] encryptedEncoded = Base64.encode(encrypted);
			// Returning base64 encoded string
			return (new String(encryptedEncoded));
	}
}
