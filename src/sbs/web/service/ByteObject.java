package sbs.web.service;

import java.io.Serializable;

import org.springframework.stereotype.Service;


@SuppressWarnings("serial")
@Service
public class ByteObject implements Serializable {
	
	private String data;
	
	private String signedData;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getSignedData() {
		return signedData;
	}

	public void setSignedData(String encmessage) {
		this.signedData = encmessage;
	}

	

}
