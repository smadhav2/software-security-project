package sbs.web.service;

import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Date;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sbs.web.dao.Otp;
import sbs.web.dao.interfaces.OtpBo;
import sbs.web.dao.interfaces.OtpDao;

@Service("OtpBo")
public class OtpService implements OtpBo {

	private static final String SECURE_ALGO = "SHA1PRNG";
	private static final int REQ_DIGITS = 8;
	private static final String DEFAULT_ALGORITHM = "HmacSHA1";
	private static final String INVALID_OTP = "invalid";
	private static final String VALID_OTP = "valid";
	private static final String EXPIRED_OTP = "expired";
	
	@Autowired
	private OtpDao otpDao;
	
	@Override
	public void saveToOtp(Otp otp) {
		// TODO Auto-generated method stub
		
		 otpDao.saveOtp(otp);
		
	}

	@Override
	public Otp returnOtp(String name) {
		// TODO Auto-generated method stub
		
		
		return otpDao.returnOtp(name);
	}
    
	public String generate() {

		String key = null;
		SecureRandom secureRandom = null;

		try {
			secureRandom = SecureRandom.getInstance(SECURE_ALGO);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		key = String.valueOf(secureRandom.nextInt(99999999));

		// Generate the OTP
		String algo = DEFAULT_ALGORITHM;

		// Generating the time based OTP code
		try {
			return generateTimeBasedOTP("" + key,
					String.valueOf(System.currentTimeMillis()),
					"" + REQ_DIGITS, algo);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// Actual computation happens here
	private String generateTimeBasedOTP(String key, String sysTime,
			String digits, String algo) {

		int numOfDigitsInOTP = Integer.decode(digits).intValue();
		String otp = null;

		while (sysTime.length() < 16) {
			sysTime = "0" + sysTime;
		}

		// Coverting hex to binary data
		byte[] keyInByte = hexToByte(key);
		byte[] sysTimeInByte = hexToByte(sysTime);

		// Generating binary data array using JCE crypto scheme
		byte[] hashedBinary = hmacSHA(keyInByte, sysTimeInByte, algo);

		// Randomly selecting the data for OTP from the hashed binary data
		int offset = hashedBinary[hashedBinary.length - 1] & 0xf;

		int binary = ((hashedBinary[offset] & 0x7f) << 24)
				| ((hashedBinary[offset + 1] & 0xff) << 16)
				| ((hashedBinary[offset + 2] & 0xff) << 8)
				| (hashedBinary[offset + 3] & 0xff);

		// Fetches the last 6 digits of the computed binary
		int otpInInt = binary % ((int) Math.pow(10, numOfDigitsInOTP));
		otp = Integer.toString(otpInInt);

		// To make the OTP sufficiently long
		if (otp.length() < numOfDigitsInOTP)
			otp = otp + "0";

		return otp;
	}

	/**
	 * This method computes the hashed authentication code.
	 * 
	 * @param keyInByte
	 *            - Keys in byte array form
	 * @param sysTimeInByte
	 *            - system time in byte array form
	 * @param algo
	 *            - crypto algorithm used to generate the hash
	 * @return hashed code in byte array
	 */
	private byte[] hmacSHA(byte[] keyInByte, byte[] sysTimeInByte, String algo) {
		try {

			// Getting an instance of the crypto algorithm specified
			Mac hmac = Mac.getInstance(algo);
			SecretKeySpec macKey = new SecretKeySpec(keyInByte, "RAW");

			// Initializing the crypto generation with the key
			hmac.init(macKey);

			return hmac.doFinal(sysTimeInByte);
		} catch (GeneralSecurityException gse) {
			throw new UndeclaredThrowableException(gse);
		}
	}

	/**
	 * @param item
	 *            - the string to be converted to hex
	 * @return byte array representation of the provided string in hex
	 */
	private byte[] hexToByte(String item) {
		// One byte to negate the effects of 2's complement
		byte[] binArray = new BigInteger("10" + item, 16).toByteArray();

		// Copy all the REAL bytes, not the "first"
		byte[] retArray = new byte[binArray.length - 1];
		for (int i = 0; i < retArray.length; i++)
			retArray[i] = binArray[i + 1];
		return retArray;
	}

	public String validateOTP(String userSubmittedOtp, Otp otpDao) {

		Date currTime = new Date();

		if (userSubmittedOtp.equals(otpDao.getOtpCode())) {
			if (currTime.getTime() - otpDao.getOtpTimeStamp() <= 300000) {
				return VALID_OTP;
			} else
				return EXPIRED_OTP;
		} else
			return INVALID_OTP;
	}

	
}
