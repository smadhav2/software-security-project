package sbs.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sbs.web.dao.User;
import sbs.web.dao.interfaces.UsersDao;
import sbs.web.dao.interfaces.UserBo;


@Service("userBo")
public class UserService  implements UserBo {
	
	
	@Autowired
	private UsersDao usersDao;
	
	

	@Override
	public User findByUserName(String username) {
		// TODO Auto-generated method stub
		return  usersDao.findUserbyUsername(username);
	}

	@Override
	public void save(User user) {
		// TODO Auto-generated method stub
		usersDao.createUser(user);
	}
	public void delete(User user){
		usersDao.deleteUser(user);
	}

	@Override
	public boolean exists(String username) {
		// TODO Auto-generated method stub
		return usersDao.exists(username);
		
	}
	
	public List<User> getAllExternalUsers(){
		
		return usersDao.getAllExternalUsers();
	}

	public void update(User user) {
		// TODO Auto-generated method stub
		usersDao.update(user);
	}

	public User getKey(String username) {
		// TODO Auto-generated method stub
		User key = usersDao.getKey(username);
		return key;
	}

	public String getPublicKey(String uname) {
		return usersDao.getPublicKey(uname);
		// TODO Auto-generated method stub
		
	}

	

	
	
	

}
