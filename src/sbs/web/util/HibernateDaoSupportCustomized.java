package sbs.web.util;


import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;


public abstract class HibernateDaoSupportCustomized extends HibernateDaoSupport{
	
	
	@Autowired
	public void session(SessionFactory sessionFactory){
		setSessionFactory(sessionFactory);
	}

	

}
