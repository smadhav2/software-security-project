package sbs.web.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sbs.web.dao.User;
import sbs.web.service.AdminService;
import sbs.web.service.UserService;

@Controller
public class SuperUserController {

	@Autowired
	private AdminService adminService;

	@Autowired
	private UserService userService;

	@RequestMapping(value="/superseupar/superuser", method = RequestMethod.GET)
	public String superUser(Model model){
		model.addAttribute("Auser", new User());
		return "superuser";
		
	}


	@RequestMapping(value="/registerAdmin",method=RequestMethod.POST)
	public String addAdmin(Model model,@Valid User user, BindingResult result){
		
		if(result.hasErrors()){
			
			return "superuser";
		}
			
		if(userService.exists(user.getUsername())){
			result.rejectValue("username", "DuplicateUsernameKey.user.username");
			return "superuser";
		}
		try{
			user.setEnabled(true);
			user.setAuthority("ROLE_ADMIN");
			userService.save(user);
		}
		catch(DuplicateKeyException ex){
			result.rejectValue("username", "DuplicateUsernameKey.user.username");
			return "superuser";
			
		}
		return "test";
	}

}
