package sbs.web.controllers;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import sbs.web.dao.Account;
import sbs.web.dao.ExternalUserDetails;
import sbs.web.dao.InternalUserRequest;
import sbs.web.dao.MakePayment;
import sbs.web.service.AccountService;
import sbs.web.service.ExternalUserDetailsService;
import sbs.web.service.InternalUserRequestService;
import sbs.web.service.MakePaymentService;
import sbs.web.service.PKIService;
import sbs.web.service.UserService;

@Controller
public class ExternalUserController {

	@Autowired
	private AccountService accountService;
	
	@Autowired
	private ExternalUserDetailsService externalUserDetailsService;
	
	@Autowired
	private UserService usersService;
	
	@Autowired 
	private InternalUserRequestService internalUserRequestService;

	@Autowired
	private MakePaymentService makePaymentService; //Test
	
	@RequestMapping(value="/secure/paymentapproval", method=RequestMethod.GET)
	public String getPaymentApproval(HttpSession session, Model model, Principal principal) {
		Authentication auth =  SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();
		session.setAttribute("username", principal.getName());
		model.addAttribute("makepayment",new MakePayment());
		model.addAttribute("merchantusername", makePaymentService.paymentApproval(principal.getName()));
				
		return "paymentapproval";
	}
	
	
	@RequestMapping(value="/secure/paymentapproved", method=RequestMethod.POST)
	public String paymentApproved(Model model, @RequestParam("paymentid") int paymentid, 
		@RequestParam(value="reject", required=false) String reject, RedirectAttributes attributes) {
		
		
		MakePayment makePayment = makePaymentService.findByPaymentId(paymentid);
		System.out.println(paymentid);
		System.out.println(makePayment.getDescription());
			
	    System.out.println(makePayment.getEncmessage());
	    
	    System.out.println(makePayment.getMessage());
	    
	    System.out.println(makePayment.getMerchantusername());
	    
		String uname=makePayment.getMerchantusername();
		
		
		PKIService pkiService=new PKIService();
		
		try {
		
			if(pkiService.verifySign(makePayment.getEncmessage(), 
			  makePayment.getMessage(), usersService.getPublicKey(uname).getBytes())==true){
				
				model.addAttribute("paymentid",paymentid);
				makePayment.setMessage(makePayment.getMessage());
			//	model.addAttribute("Transactionmessage",makePayment.getMessage()+", Approved");
				
				model.addAttribute("payment",makePayment);
				attributes.addFlashAttribute("message", "Your payment is verified and approved");
				return "paymentapproved";
			}
			
			else{
				makePayment.setApproved(2);
				makePaymentService.updateApproved(makePayment);
				attributes.addFlashAttribute("message", "rejected the payment from "+makePayment.getMerchantusername());
				//attributes.addFlashAttribute("message", "Your request from"+makePayment.getMerchantusername()+
					//	" is not verified contact with merchant");
				
				return "redirect:/secure/paymentapproval";
			}
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		} catch (SignatureException e) {
			e.printStackTrace();
		}
			
			
		return "redirect:/secure/paymentapproval";
		
	}
	@RequestMapping(value="/secure/paymentapproved", method=RequestMethod.GET)
		public String getPaymentApproved(Model model,RedirectAttributes attr){
			attr.addFlashAttribute("error", "Cannot reach this page through post");
		
		return "redirect:/secure/paymentapproval";
	}
	
	@RequestMapping(value="/secure/paymentapproval", method=RequestMethod.POST)
	public String getPaymentApproval( Model model, Principal principal, @RequestParam("paymentid") int paymentid,
			@RequestParam("submit")String button,	RedirectAttributes redirectAttributes, MakePayment mP,
			@RequestParam("plainmessage")String plainmessage) {
		
		MakePayment makePayment = makePaymentService.findByPaymentId(mP.getPaymentid());
		model.addAttribute("paymentid",paymentid);
		model.addAttribute("payment",makePayment);
		
		if(button.equals("Encrypt & Approve")){
			
			PKIService pkiService=new PKIService();
			
			plainmessage=plainmessage+", Approved";
			
			try {
				makePayment.setUsermessage(pkiService.encrypt(plainmessage.getBytes(),usersService.getPublicKey("sbsbank").getBytes()));
			} catch (InvalidKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalBlockSizeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (BadPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		makePaymentService.updateApproved(makePayment);	
		
		   return "paymentfromuser";
		}
		
		else{
	    makePayment.setApproved(2);
		makePaymentService.updateApproved(makePayment);
		redirectAttributes.addFlashAttribute("message", "rejected the payment from "+makePayment.getMerchantusername());
	
		return "redirect:/secure/paymentapproval";
		}
	}
//	@RequestMapping(value="/secure/paymentfromuser", method=RequestMethod.GET)
//	public String getPaymentFromUser() {
//		return "paymentfromuser";
//	}
	@RequestMapping(value="/secure/paymentfromuser", method=RequestMethod.POST)
	public String paymentFromUser(MakePayment mP,Model model,@RequestParam("paymentid") int paymentid,
			@RequestParam("userencryptedmessage") String encmessage){
		
		MakePayment makePayment = makePaymentService.findByPaymentId(mP.getPaymentid());
		
		makePayment.setApproved(1);
		
		makePayment.setUserencryptedmessage(encmessage);
		
		System.out.println(makePayment.toString());
		
		makePaymentService.updateApproved(makePayment);
		
		return "redirect:/secure/paymentapproval";
		
		
	}
	
	
	@RequestMapping(value="/secure/createaccount" , method=RequestMethod.GET)
	public String createAccount(Model model){
		model.addAttribute("account", new Account());
		return "createaccount";
	}
	
	@RequestMapping(value="/secure/createaccount", method = RequestMethod.POST)
	public String createAccount(Model model,Account account,BindingResult result, 
			RedirectAttributes redirectAttributes){
		Authentication auth =  SecurityContextHolder.getContext().getAuthentication();
		account.setUsername(auth.getName());
		try{
			accountService.isAccountExists(auth.getName(),account.getAccountType());
		}catch(DuplicateKeyException ex){
			result.rejectValue("accountType", "DuplicateAccountType", ex.getMessage());
			redirectAttributes.addFlashAttribute("error", ex.getMessage());
			return "redirect:/secure/createaccount";
		}
		
		accountService.createAccount(account);
				
		return "redirect:/secure/createaccount";
	}
	
	@RequestMapping(value="/secure/updateuserdetails", method = RequestMethod.GET)
	public String createOrUpdateUserDetails(Model model,@RequestParam(value="message",required=false)boolean message, 
			Principal principal){
		
		ExternalUserDetails externalUserDetails = null;
		if(principal != null){
			String username=principal.getName();
			externalUserDetails = externalUserDetailsService.getExternalUserDetails(username);
		}
		if(externalUserDetails==null){
			externalUserDetails = new ExternalUserDetails();
		}
		model.addAttribute("externalUserDetails", externalUserDetails);
		
		return "externaluserdetails";
	}
	
	@RequestMapping(value="/secure/updateuserdetails", method = RequestMethod.POST)
	public String createOrUpdateDetails(Model model,ExternalUserDetails externalUserDetails, 
			RedirectAttributes redirectAttributes){
		
		Authentication auth =  SecurityContextHolder.getContext().getAuthentication();
		
			String username = auth.getName();
			externalUserDetails.setUsername(username);
			if(externalUserDetailsService.externalUserDetailsExist(username)){
				externalUserDetails.setExternaluserdetialid(externalUserDetailsService.getExternalUserid(username));
				externalUserDetailsService.updateExternalUserDetails(externalUserDetails);
				redirectAttributes.addFlashAttribute("message", "your data has been updated");
			}
			else{
			externalUserDetailsService.saveExternalUserDetails(externalUserDetails);
			redirectAttributes.addFlashAttribute("message", "your data has been saved");
			}
			
			
		return "redirect:/secure/updateuserdetails";
	}
	
	@RequestMapping(value="/secure/banknotifications", method= RequestMethod.GET)
	public String viewNotifications(Model model, Principal principal ){
		
		model.addAttribute("isr", new InternalUserRequest());
		model.addAttribute("requests", internalUserRequestService.getAllInternalUserRequests(principal.getName()));
		
		return "notifications";
	}
	@RequestMapping(value="/secure/banknotifications", method= RequestMethod.POST)
	public String viewNotifications(Model model, InternalUserRequest request, @RequestParam(value ="submit", required=false) String tReview,
			@RequestParam(value="access", required = false)String access,Principal principal,RedirectAttributes redirectAttributes){
		
		int status = 0;
		String flash="";
		String permission="";
		if(access.equals("approve")){
			status = 1;
			flash = "message";
			permission ="granted ";
			
		}else if(access.equals("deny")){
			status =2;
			flash="notify";
			permission ="denied ";
			
		}
		
		switch(tReview){
		case "transactionReview": 
			if(internalUserRequestService.exists(request.getInternalUsername(),principal.getName())){
				request = internalUserRequestService.find(request.getInternalUsername(), principal.getName());
				request.setTransactionalReview(status);
				internalUserRequestService.update(request);
				redirectAttributes.addFlashAttribute(flash, "You have "+ permission + request.getInternalUsername() +" permission to perform transaction review");
			}
			
			break;
		case "userDetail": 
			if(internalUserRequestService.exists(request.getInternalUsername(),principal.getName())){
				request = internalUserRequestService.find(request.getInternalUsername(), principal.getName());
				request.setUserDetails(status);
				internalUserRequestService.update(request);
				redirectAttributes.addFlashAttribute(flash, "You have "+ permission + request.getInternalUsername() +" permission to view user details");
			}
			break;
		case "accountDetail":
			if(internalUserRequestService.exists(request.getInternalUsername(),principal.getName())){
				request = internalUserRequestService.find(request.getInternalUsername(), principal.getName());
				request.setAccountDetails(status);
				internalUserRequestService.update(request);
				redirectAttributes.addFlashAttribute(flash, "You have " + permission + request.getInternalUsername() +" permission to perform account review");
			}
			break;
		
			}
		
		
	
	
	return "redirect:/secure/banknotifications";
}
	}
