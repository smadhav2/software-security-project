package sbs.web.controllers;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import sbs.web.dao.Account;
import sbs.web.dao.Transaction;
import sbs.web.service.AccountService;
import sbs.web.service.TransactionService;

@Controller
public class TransactionController {

	@Autowired
	private AccountService accountService;
	
	@Autowired
	private TransactionService tranService;

	@RequestMapping(value="/secure/viewTransaction", method=RequestMethod.GET)
	public String showAdmin(Model model, Principal principal){
		
		List<Account> accs = accountService.getAforU(principal.getName());
		Map<Account, List<Transaction>> map = new HashMap<Account,List< Transaction>>();
		for(Account acc : accs){
		
		if(acc!=null){
		
		
		List<Transaction> t = tranService.getTforA(acc.getAccountNumber());
		map.put(acc, t);
		}
		}
		model.addAttribute("map", map);
		model.addAttribute("tran", new Transaction());
		return "transaction";
	}
	@RequestMapping(value="/secure/viewTransaction", method=RequestMethod.POST)
	public String showAdmin(Model model, RedirectAttributes redirectAttributes, @RequestParam(value="tid")int tid){
		Transaction tran = tranService.findByTid(tid);
		if(tran!=null){
			tran.setStatus("Review");
			tranService.update(tran);
			
		}else{
			redirectAttributes.addFlashAttribute("error", "No such transaction exists");
		}
		
		
		return "redirect:/secure/viewTransaction";
	}

	
	@RequestMapping(value="/secure/transfer", method=RequestMethod.GET)
	public String showTransfers(Model model, Principal principal){
		model.addAttribute("Accounts", accountService.getAforU(principal.getName()));
		model.addAttribute("newTran", new Transaction());
		return "tprocess";
	}
	
	
	@RequestMapping(value="/secure/transfer",method=RequestMethod.POST)
	public String doCredit(Model model, Transaction tran, BindingResult result, RedirectAttributes redirectAttributes){
		
		tran.setType("Debit");
	
		Account acc = accountService.getAforA(tran.getAccountNumber());
		
		Account acc2 = accountService.getAforA(tran.getOtherparty());
		
		int k =acc.getAccountNumber();
		tran.setAccountNumber(k);
		
		int p = tran.getAccountNumber();
		System.out.println(p);
		if(tran.getAmount() < 10000)
		{
		if(acc.getAccountNumber() != tran.getOtherparty())
		{
			try{
				tranService.fundTransfer(acc,acc2,tran,tran.getAmount(),tran.getType());
				
				tran.setStatus("Approved");
				
				redirectAttributes.addFlashAttribute("message", "Transfer Successful");
			}catch(NumberFormatException ex){
				redirectAttributes.addFlashAttribute("error",ex);
				return "redirect:/secure/transfer";
			}
			}
		
		}
		else{
			tran.setStatus("Pending");
			redirectAttributes.addFlashAttribute("message", "Critical Transaction..! Please be patient till we procss it.  ");
						
		}
		
		Transaction newtran = new Transaction();
		
		newtran.setAccountNumber(tran.getOtherparty());
		newtran.setOtherparty(tran.getAccountNumber());
		newtran.setAmount(tran.getAmount());
		newtran.setStatus(tran.getStatus());
		newtran.setType("Credit");
		
		tranService.createTran(tran);
		tranService.createTran(newtran);
		return "redirect:/secure/transfer";
	}
	
	@RequestMapping(value="/secure/atm", method= RequestMethod.GET)
	public String creditDebit(Model model, Principal principal){
		model.addAttribute("Accounts", accountService.getAforU(principal.getName()));
		model.addAttribute("newTran", new Transaction());
		return "creditdebit";
	}
	@RequestMapping(value="/secure/atm", method= RequestMethod.POST)
	public String creditDebit(Model model, Transaction tran, BindingResult result, RedirectAttributes redirectAttributes){
		
		String DCcheck = tran.getType();
		String DCcheck2 ="Credit";
		System.out.println(DCcheck);
	
		Account acc = accountService.getAforA(tran.getAccountNumber());
		
		
		int k =acc.getAccountNumber();
		tran.setOtherparty(k);
		
		
		if(tran.getAmount() < 10000)
		{
		
			if(DCcheck.equals(DCcheck2)){
			try{
				tranService.doCredit(acc, tran.getAmount());
			tran.setStatus("Approved");
			redirectAttributes.addFlashAttribute("message", "Credit Successfull");
			}catch(NumberFormatException ex){
				redirectAttributes.addFlashAttribute("error",ex.getMessage());
				return "redirect:/secure/atm";
			}
			}
			else
			{
				try{
				tranService.doDebit(acc, tran.getAmount());
				tran.setStatus("Approved");
				redirectAttributes.addFlashAttribute("message", "Debit Successfull");
				}catch(NumberFormatException ex){
					redirectAttributes.addFlashAttribute("error", ex.getMessage());
					return "redirect:/secure/atm";
				}
			}
		
		
	}else{
		tran.setStatus("Pending");
		redirectAttributes.addFlashAttribute("message", "Critical Transaction..! Please be patient till we procss it.  ");
					
	}
		tranService.createTran(tran);
		return "redirect:/secure/atm";
	}
}
