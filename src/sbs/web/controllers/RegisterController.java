package sbs.web.controllers;

import java.io.File;
import java.net.URL;

import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

import sbs.web.dao.User;
import sbs.web.service.PKIService;
import sbs.web.service.UserService;

@Controller
public class RegisterController {

	private UserService userService;
	private PKIService pkiService;

	@Autowired
	private JavaMailSender mailSender;

	private static final Logger logger = Logger
			.getLogger(RegisterController.class);

	@Autowired
	public void setPkiService(PKIService pkiService) {
		this.pkiService = pkiService;
	}

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@RequestMapping("/register")
	public String register(Model model) {
		System.out.println("Krishan");
		// logs debug message
			logger.debug("register is executed!");
		model.addAttribute("user", new User());
		return "register";
	}

	@RequestMapping(value = "/doregister", method = RequestMethod.POST)
	public String doRegister(Model model, @Valid User user, BindingResult result) {
		if (result.hasErrors()) {
			return "register";
		}

		if (userService.exists(user.getUsername())) {
			result.rejectValue("username", "DuplicateUsernameKey.user.username");
			return "register";
		}
		System.out.println("Krishan");
		// logs debug message
		logger.debug("doRegister is executed!");
		try {
			int authid = (int) (100 * Math.random());
			user.setAuthid(authid);
			user.setAuthority("ROLE_EXTERNALUSER");
			pkiService.initializePair();
			user.setPublicKey(new String(Base64.encode(pkiService.getPair().getPublic().getEncoded())));
			logger.debug("doRegister is executed!");
			final String un = user.getUsername();
			final String email = user.getEmail();
			mailSender.send(new MimeMessagePreparator() {
				public void prepare(MimeMessage mimeMessage)
						throws MessagingException {
					
					MimeMessageHelper message = new MimeMessageHelper(
							mimeMessage, true, "UTF-8");
					message.setFrom("xxxxx@sbs.com");
					message.setTo(email);
					message.setText("Keep you private key secure. Please download the Jar File and change the extension to .jar");
					message.setSubject("Your private key from sbs bank.");
					message.addAttachment("private key", pkiService
							.getPrivateKey(un, pkiService.getPair()
									.getPrivate()));
					
					FileDataSource fds;
					try {
						URL url = getClass().getResource("sign-decrypt.jar");
						File file = new File(url.getPath());
						fds = new FileDataSource(file);
						message.addAttachment("JAR File", fds);
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println("Signing-Decrypting utility not sent. Please contact admin");
					}
				}
			});
			
			userService.save(user);
		} catch (DuplicateKeyException ex) {
			result.rejectValue("username", "DuplicateUsernameKey.user.username");
			return "register";
		}finally{
			pkiService.deleteFile(user.getUsername());
		}

		return "test";
	}

}
