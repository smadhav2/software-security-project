package sbs.web.controllers;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;

import javax.servlet.http.HttpSession;

import sbs.web.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;



import sbs.web.dao.Account;
import sbs.web.dao.InternalUserDetails;
import sbs.web.dao.MakePayment;
import sbs.web.dao.PaymentToBank;
import sbs.web.dao.Transaction;
import sbs.web.service.AccountService;
import sbs.web.service.InternalUserDetailsService;
import sbs.web.service.MakePaymentService;
import sbs.web.service.PKIService;
import sbs.web.service.PaymentToBankService;
import sbs.web.service.UserService;



@Controller
public class InternalUserController {
	
	@Autowired
	private InternalUserDetailsService internalUserDetailsService;
	
	@Autowired
	private MakePaymentService makePaymentService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private TransactionService tranService;
	
	@Autowired
	private PaymentToBankService paymentToBankService;
	
	@RequestMapping(value="/secure/updateinuserdetails", method = RequestMethod.GET)
	public String createOrUpdateUserDetails(Model model,@RequestParam(value="message",required=false)boolean message, Principal principal){
		InternalUserDetails internalUserDetails = null;
		if(principal != null){
			String username=principal.getName();
			internalUserDetails = internalUserDetailsService.getInternalUserDetails(username);
		}
		if(internalUserDetails==null){
			internalUserDetails = new InternalUserDetails();
		}
		model.addAttribute("internalUserDetails", internalUserDetails);
		
		return "internaluserdetails";
	}
	
	@RequestMapping(value="/secure/updateinuserdetails", method = RequestMethod.POST)
	public String createOrUpdateDetails(Model model,InternalUserDetails internalUserDetails, RedirectAttributes redirectAttributes){
		Authentication auth =  SecurityContextHolder.getContext().getAuthentication();
		
			String username = auth.getName();
			internalUserDetails.setUsername(username);
			if(internalUserDetailsService.internalUserDetailsExist(username)){
				internalUserDetails.setInternaluserdetialid(internalUserDetailsService.getInternalUserid(username));
				internalUserDetailsService.updateInternalUserDetails(internalUserDetails);
				redirectAttributes.addFlashAttribute("message", "your data has been updated");
			}
			else{
			internalUserDetailsService.saveInternalUserDetails(internalUserDetails);
			redirectAttributes.addFlashAttribute("message", "your data has been saved");
			}
			
			
		return "redirect:/secure/updateinuserdetails";
	}
	@RequestMapping(value="/admin/bankapproval", method=RequestMethod.POST)
	public String bankApproval(@RequestParam(value="mmessage", required=false)String mmessage,@RequestParam(value="submit")String submit,RedirectAttributes redirectAttributes, @RequestParam(value="umessage", required=false)String umessage, @RequestParam(value="paymentid")int paymentid){
		if(submit.equals("Approve")){
			System.out.println(mmessage);
			System.out.println(umessage);
			String[] something= umessage.split(", ");
			
			
			if((mmessage.trim()).equals((something[0]).trim())){
				String[] something2 = mmessage.split(" ");
				if(userService.exists(something2[1])){
					Account acc= accountService.getAforUCheckings(something2[1]);
					Account acc2 = accountService.getAforUCheckings(something2[5]);
					if(!something[1].equals("Approved")){
						MakePayment mk = makePaymentService.findByPaymentId(paymentid);
						mk.setApproved(6);
						makePaymentService.update(mk);
						PaymentToBank pk1 = paymentToBankService.getEncryptedMessage(paymentid, something2[1]);
						PaymentToBank pk2 = paymentToBankService.getEncryptedMessage(paymentid, something2[5]);
						paymentToBankService.delete(pk1);
						paymentToBankService.delete(pk2);
						redirectAttributes.addAttribute("error", "Not Approved by user...deleted");
						return "redirect:/secure/paymentfrommerchant";
						}
					
					int amount= Integer.parseInt(something2[3].trim());
					System.out.println(amount);
					System.out.println(acc.getAccountBalance());
					if(acc.getAccountBalance()-amount > 0){
					Transaction tran = new Transaction();
					Transaction tran2 = new Transaction();
					tran.setAccountNumber(acc.getAccountNumber());
					tran.setOtherparty(acc2.getAccountNumber());
					tran.setStatus("Approved");
					tran.setType("Debit");
					tran2.setAccountNumber(acc2.getAccountNumber());
					tran2.setOtherparty(acc.getAccountNumber());
					tran2.setStatus("Approved");
					tran2.setType("Credit");
					tran2.setAmount(amount);
					tran.setAmount(amount);
					tranService.fundTransfer(acc, acc2, tran, amount, "Debit");
					tranService.createTran(tran);
					tranService.createTran(tran2);
					
					MakePayment mk = makePaymentService.findByPaymentId(paymentid);
					mk.setApproved(5);
					makePaymentService.update(mk);
					PaymentToBank pk1 = paymentToBankService.getEncryptedMessage(paymentid, something2[1]);
					PaymentToBank pk2 = paymentToBankService.getEncryptedMessage(paymentid, something2[5]);
					paymentToBankService.delete(pk1);
					paymentToBankService.delete(pk2);
					return "test";
					}
					else{
						redirectAttributes.addFlashAttribute("The user does not have sufficient balance...");
					}
					
				}
				System.out.println("hello");
			}
		}else{
			System.out.println(mmessage);
			System.out.println(umessage);
			String[] something= umessage.split(", ");
			if((mmessage.trim()).equals((something[0]).trim())){
				String[] something2 = mmessage.split(" ");
				if(userService.exists(something2[1])){
					
					MakePayment mk = makePaymentService.findByPaymentId(paymentid);
					mk.setApproved(6);
					makePaymentService.update(mk);
					PaymentToBank pk1 = paymentToBankService.getEncryptedMessage(paymentid, something2[1]);
					PaymentToBank pk2 = paymentToBankService.getEncryptedMessage(paymentid, something2[5]);
					paymentToBankService.delete(pk1);
					paymentToBankService.delete(pk2);
					
					}
					else{
						redirectAttributes.addFlashAttribute("The user does not have sufficient balance...");
					}
					
				}
				System.out.println("hello");
			}
			
		return "redirect:/admin/paymentfrommerchant";
		
		
		
	}
	
	@RequestMapping(value="/admin/paymentfrommerchant",method=RequestMethod.GET)
	public String getPaymentFromMerchant(Model model, HttpSession session, Principal principal) {
		session.setAttribute("username", principal.getName());
		model.addAttribute("makepayment",new MakePayment());
		model.addAttribute("paymentfrommerchant", makePaymentService.paymentFromMerchant());
		return "paymentfrommerchant";
	}
	
	@RequestMapping(value="/admin/paymentfrommerchant",method=RequestMethod.POST)
	public String approvedPaymentFromMerchant(Model model, HttpSession session, Principal principal,
			RedirectAttributes redirect, @RequestParam("paymentid") int paymentid, @RequestParam(value="reject", required=false)String reject) {
			String message="";
			
		MakePayment makePayment=makePaymentService.findByPaymentId(paymentid);	
	    makePayment.getMerchantusername();
	    makePayment.getUsername();
		
		String merchantKey=userService.getPublicKey(makePayment.getMerchantusername());
		String UserKey=userService.getPublicKey(makePayment.getUsername());
		PaymentToBank pk1 = paymentToBankService.getEncryptedMessage(paymentid,makePayment.getMerchantusername());
		PaymentToBank pk2 = paymentToBankService.getEncryptedMessage(paymentid,makePayment.getUsername());
		String merchantEncMessage=paymentToBankService.getEncryptedMessage(paymentid,makePayment.getMerchantusername()).getEncryptedmessage();
		String userEncMessage=paymentToBankService.getEncryptedMessage(paymentid,makePayment.getUsername()).getEncryptedmessage();
		
		String merchantMessage=paymentToBankService.getEncryptedMessage(paymentid,makePayment.getMerchantusername()).getMessage();
		String userMessage=paymentToBankService.getEncryptedMessage(paymentid,makePayment.getUsername()).getMessage();
		System.out.println("merchantMessage:  "+merchantMessage);
		System.out.println("    merchantEncMessage:  "+merchantEncMessage);
		System.out.println(merchantEncMessage);
		System.out.println(userEncMessage);
		
		PKIService pkiService=new PKIService();
		
	  try {
		
		boolean verifyMerchant=pkiService.verifySign(merchantEncMessage, merchantMessage, merchantKey.getBytes());
		boolean verifyUser=pkiService.verifySign(userEncMessage, userMessage, UserKey.getBytes());
		
		System.out.println(verifyMerchant);
		System.out.println(verifyUser);
		
		if(verifyMerchant && verifyUser){
			System.out.println("Hi");
			model.addAttribute("pk1", pk1);
			model.addAttribute("pk2", pk2);
			return "bankapproval";
		}
		
	} catch (InvalidKeyException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (InvalidKeySpecException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SignatureException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
			
//		session.setAttribute("username", principal.getName());
//		model.addAttribute("makepayment",new MakePayment());
//		MakePayment makePayment = makePaymentService.findByPaymentId(paymentid);
//		
//		if(reject==null) {
//			makePayment.setApproved(5);
//			message="Transaction submitted";
//		}
//		else {
//			makePayment.setApproved(4);
//			message="Transaction declined";
//		}
//			redirect.addFlashAttribute("message", message);
//			makePaymentService.updateApproved(makePayment);
			
			
			
			
		return "redirect:/admin/paymentfrommerchant";
	}

}
