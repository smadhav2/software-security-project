package sbs.web.controllers.exceptionhandlers;

import org.springframework.dao.DataAccessException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class DatabaseErrorHandler {

	@ExceptionHandler(DataAccessException.class)
	public String handleDatabaseExceptions(DataAccessException ex){
		
		return "error";
	}
	public String handleAccessDeniedExceptions(AccessDeniedException ex){
		return "redirect:/403";
	}
	
}
