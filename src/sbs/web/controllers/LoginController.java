package sbs.web.controllers;

import java.security.Principal;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import sbs.web.dao.Otp;
import sbs.web.dao.User;
import sbs.web.service.OtpService;
import sbs.web.service.UserAttemptsService;
import sbs.web.service.UserService;

@Controller
public class LoginController {

	@Autowired
	private UserService usersService;

	@Autowired
	private MailSender mailSender;

	@Autowired
	private UserAttemptsService userAttemptsService;
	
	@Autowired
	private OtpService otpservice;

	private static final Logger logger = Logger
			.getLogger(LoginController.class);

	@RequestMapping("/login")
	public String showLogin(
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,
			HttpServletRequest request, Model model) {

		if (error != null) {
			model.addAttribute("error",
					getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
			logger.error("This is Error message", new Exception(
					"SPRING_SECURITY_LAST_EXCEPTION"));
		}
		if (logout != null) {
			model.addAttribute("msg", "You've been logged out successfully.");
		}
		return "login";
	}

	private String getErrorMessage(HttpServletRequest request, String key) {

		Exception exception = (Exception) request.getSession()
				.getAttribute(key);

		String error = "";
		if (exception instanceof BadCredentialsException) {
			error = exception.getMessage();
		} else if (exception instanceof LockedException) {
			error = exception.getMessage();
		} else {
			error = "User has not yet been enabled";
		}

		return error;
	}

	@RequestMapping(value="/login", method = RequestMethod.POST)
	public String newPasswordPost(Model model,@RequestParam (value="username") String uname, @RequestParam (value="password")String newpassword, RedirectAttributes redirectAttributes){
		
		//System.out.println(usersService.exists(uname));
		if(usersService.exists(uname)){
		System.out.println("i am here");
			
		User user = usersService.findByUserName(uname);
		user.setPassword(newpassword);
		usersService.update(user);
		redirectAttributes.addFlashAttribute("message", "Your password has been updated");
		}
		else{
			redirectAttributes.addFlashAttribute("message", "Your password has not been updated");
		}
		return "redirect:/login";
		
	}

//	@RequestMapping(value = "/forgotpassword", method = RequestMethod.POST)
//	public String forgotPassword(Model model,
//			@RequestParam(value = "username") String username) {
//		// logs debug message
//		if (logger.isDebugEnabled()) {
//			logger.debug("forgotPassword is executed!");
//		}
//		User user = usersService.findByUserName(username);
//		String message = "This is your new password";
//		SimpleMailMessage mail = new SimpleMailMessage();
//		mail.setTo(user.getEmail());
//		mail.setFrom("xxxxx@sbs.com");
//		mail.setSubject("Test message");
//		mail.setText(message);
//
//		try {
//			mailSender.send(mail);
//			model.addAttribute("msg", "Your mail has been sent");
//		} catch (Exception e) {
//			e.printStackTrace();
//			System.out.println("Can't send message");
//		}
//		return "login";
//
//	}

	// for 403 access denied page
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public String accesssDenied(Model model) {

		// check if user is login
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			model.addAttribute("username", userDetail.getUsername());

		}

		return "403";

	}

	@RequestMapping("/logout")
	public String showLoggedOut(Model model, HttpSession session) {
		// logs debug message
		if (logger.isDebugEnabled()) {
			logger.debug("showLoggedOut is executed!");
		}
		model.addAttribute("status", false);
		session.invalidate();
		return "home";
	}

	@RequestMapping("/secure/dologin")
	public String doLogin(HttpSession session, Principal principal) {
		// logs debug message
		if (logger.isDebugEnabled()) {
			logger.debug("doLogin is executed!");
		}
		session.setAttribute("username", principal.getName());
		return "userhome";

	}

	@RequestMapping(value="/forgotpassword", method = RequestMethod.GET)
	public String forgotPassword(Model model){
		//model.addAttribute("otp", new Otp());
		return "forgotpassword";
		
	}
	
	
	@RequestMapping(value="/forgotpassword/otp", method = RequestMethod.POST)
	public String forgotPassword(Model model,@RequestParam (value="username") String username,Otp otp, RedirectAttributes redirect ){
		
		String otpValue=otpservice.generate();
		if(usersService.exists(username)){
		User user= usersService.findByUserName(username);
		model.addAttribute("username", username);
		
		
		
		Otp otpObj=new Otp();
		Date currTime = new Date();
		
		otpObj.setOtpCode(otpValue);
		otpObj.setOtpTimeStamp(currTime.getTime());
		otpObj.setUsername(username);
		System.out.println("Hello this is otp"+ otpObj);
		otpservice.saveToOtp(otpObj);
		
		String message = "This is your otp for forgot password:   "+otpValue ;
		SimpleMailMessage mail =new SimpleMailMessage();
		mail.setTo(user.getEmail());
		mail.setFrom("xxxxx@sbs.com");
		mail.setSubject("Test message");
		mail.setText(message);
		
		try {
			mailSender.send(mail);
			model.addAttribute("msg", "Your mail has been sent");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Can't send message");
		}
		
		return "otp";
		}
		 
		redirect.addFlashAttribute("error","Please enter correct user name");
		return "redirect:/forgotpassword";
	}
	
	@RequestMapping(value="/forgotpassword/otp", method = RequestMethod.GET)
	public String otpGet(Model model){
		
		return "otp";
		
	}
	
	@RequestMapping(value="/forgotpassword/otp/newpassword", method = RequestMethod.POST)
	public String otpPost(Model model,@RequestParam (value="otp") String otpval,
			@RequestParam (value="username") String uname,  
			RedirectAttributes redirectAttributes){
		
		
		
		if(otpservice.validateOTP(otpval, otpservice.returnOtp(uname)) == "valid"){
			model.addAttribute("username", uname);
		return "newpassword";
		
		}
		else{
			redirectAttributes.addFlashAttribute("message", "Please try again");
			
			return "redirect:/forgotpassword";
		}
		
	}
	
	
	@RequestMapping(value="/forgotpassword/otp/newpassword", method = RequestMethod.GET)
	public String newPassword(Model model){
	
		return "newpassword";
		
	}
}
