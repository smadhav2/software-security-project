package sbs.web.controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import sbs.web.dao.Account;
import sbs.web.dao.InternalUserRequest;
import sbs.web.dao.MakePayment;
import sbs.web.dao.PaymentToBank;
import sbs.web.dao.Transaction;
import sbs.web.dao.User;
import sbs.web.dao.UserAttempts;
import sbs.web.service.AccountService;
import sbs.web.service.AdminService;
import sbs.web.service.ByteObject;
import sbs.web.service.InternalUserRequestService;
import sbs.web.service.MakePaymentService;
import sbs.web.service.PKIService;
import sbs.web.service.PaymentToBankService;
import sbs.web.service.TransactionService;
import sbs.web.service.UserAttemptsService;
import sbs.web.service.UserService;

@Controller
public class AdminHomeController {

	private static final int KEY_SIZE = 1024;

	@Autowired
	private AdminService adminService;

	@Autowired
	private UserService userService;
	
	@Autowired
	private UserAttemptsService userAttemptsService;
	
	@Autowired
	private MakePaymentService makePaymentService;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private InternalUserRequestService internalUserRequestService;
	
	@Autowired
	private TransactionService transService; 
	
	@Autowired
	private PaymentToBankService paymentToBankService;
	
	@RequestMapping(value="/secure/makepayment",method=RequestMethod.GET)
	public String getMakePayment(Model model, HttpSession session, Principal principal){
		
		session.setAttribute("mechantusername", principal.getName());
		session.setAttribute("username", makePaymentService.getExternalUser());
		model.addAttribute("makepayment", new MakePayment());
		return "makepayment"; 
	}
	
	@RequestMapping(value="/secure/submitpayment",method=RequestMethod.POST)
	public String getSubmitPayment(Model model,MakePayment makepayment, @RequestParam("message") String message, 
			 @RequestParam("username") String username,RedirectAttributes attributes,HttpSession session,Principal principal){
		
		Authentication auth =  SecurityContextHolder.getContext().getAuthentication();
		
		makepayment.setMerchantusername(auth.getName());
		
		session.setAttribute("mechantusername", principal.getName());
		PKIService service=new PKIService();
		
		try {
           
			System.out.println(message);
			System.out.println( userService.getPublicKey(username));
			
    		makepayment.setMessage(service.encrypt(message.getBytes(), userService.getPublicKey(username).getBytes()));
    		
    		System.out.println(service.encrypt(message.getBytes(), userService.getPublicKey(username).getBytes()));
			
			model.addAttribute("encrypt",service.encrypt(message.getBytes(), userService.getPublicKey(username).getBytes()));
			PaymentToBank pb = new PaymentToBank();
			pb.setMessage(service.encrypt(message.getBytes(), userService.getPublicKey("sbsbank").getBytes()));
			model.addAttribute("pb",pb);
			model.addAttribute("encryptToBank",service.encrypt(message.getBytes(), userService.getPublicKey("sbsbank").getBytes()));
			
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| InvalidKeySpecException | NoSuchPaddingException
				| IllegalBlockSizeException | BadPaddingException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		model.addAttribute("makepayment",makepayment);
		makePaymentService.submitPaymentToUser(makepayment);
		
		
		return "signfile";
	}
	
	
	@RequestMapping(value="/secure/signfile",method=RequestMethod.POST)
	public String signPayment(Model model,RedirectAttributes attributes, 
			@RequestParam("msg2") String encryptedmessage2,@RequestParam("msg1") String message2,MakePayment makepayment){
		
		PaymentToBank pb = new PaymentToBank();
		MakePayment mk =  makePaymentService.getMakePayment(makepayment.getPaymentid());
		mk.setEncmessage(makepayment.getEncmessage());
		
		pb.setEncryptedmessage(encryptedmessage2);
		pb.setMessage(message2);
		pb.setAuthority("ROLE_MERCHANT");
		pb.setUsername(mk.getMerchantusername());
		pb.setPaymentid(mk.getPaymentid());
		
		System.out.println("From merchanr enc:   " + pb.getEncryptedmessage());
		System.out.println("From merchant msg:    " + pb.getMessage());
		paymentToBankService.savePayment(pb);
		
		makePaymentService.update(mk);
		
		attributes.addFlashAttribute("message", "Your payment is submitted to the user");
		
		return "userhome";
	}
	
	@RequestMapping(value="/secure/newhome", method = RequestMethod.GET)
	public String getTestPage(Model model, HttpSession session, Principal principal){
		Authentication auth =  SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();
		model.addAttribute("username", principal.getName());
		model.addAttribute("makepayment",new MakePayment());
		model.addAttribute("merchantusername", makePaymentService.paymentApprovedByUser(username));
		model.addAttribute("merchantusernamereject", makePaymentService.paymentRejectedByUser(username));
		model.addAttribute("pending", makePaymentService.submitPaymentToBank(username));
		model.addAttribute("reject", makePaymentService.payemntRejectedByBank(username));
		return "test2";
	}
	
	@RequestMapping(value="/secure/newhome", method = RequestMethod.POST)
	public String sendToBank(Model model, HttpSession session, Principal principal,
			RedirectAttributes redirect, @RequestParam("paymentid") int paymentid){

		session.setAttribute("merchantusername", principal.getName());
		model.addAttribute("makepayment",new MakePayment());
		
		String message="Payment sent to bank";
		redirect.addFlashAttribute("message", message);
		
		MakePayment makePayment = makePaymentService.findByPaymentId(paymentid);
		
		makePayment.getUserencryptedmessage();
		makePayment.getUsermessage();
		
		PaymentToBank paymentToBank = new PaymentToBank();
		paymentToBank.setEncryptedmessage(makePayment.getUserencryptedmessage());
		paymentToBank.setMessage(makePayment.getUsermessage());
		paymentToBank.setAuthority("ROLE_USER");
		paymentToBank.setPaymentid(makePayment.getPaymentid());
		paymentToBank.setUsername(makePayment.getUsername());
		
		paymentToBankService.savePayment(paymentToBank);
		makePayment.setApproved(3);
		makePaymentService.updateApproved(makePayment);
		return "redirect:/secure/newhome";
	}
	@RequestMapping(value="/admin/newhome", method = RequestMethod.GET)
	public String showadmin(Model model){
		model.addAttribute("user", new User());
		model.addAttribute("disabledUser", adminService.getDisabledUser());
		model.addAttribute("lockedUser",adminService.getLockedUser());
		model.addAttribute("tranPen",adminService.getPendingTransactions());
		return "admin";
	}
	
	@RequestMapping("/admin/register")
	public String startInternalUserRegister(Model model){
		model.addAttribute("user", new User());
		return "Cregister";
	}
	
	@RequestMapping(value="/admin/newhome" , method=RequestMethod.POST)
	public String EnableUser(Model model, @RequestParam("enabled") String username, @RequestParam(value="delete", required=false)String delete,
			final RedirectAttributes redirectAttributes)
	{
		User user = userService.findByUserName(username);
		String message = null;
		if(delete==null){
		
		user.setEnabled(true);
		adminService.enableUser(user);
		message="User has been enabled";
		}
		else{
			
			if(userService.exists(username)){
			adminService.delete(user);
			message="User has been deleted";
			}else{
				message = "User has already been deleted by another admin";
			}
		}
		redirectAttributes.addFlashAttribute("message", message);
		return "redirect:/admin/newhome";
	}
	@RequestMapping(value="/admin/unlockuser" , method=RequestMethod.POST)
	public String unLockUser(Model model, @RequestParam("enabled") String username, @RequestParam(value="delete", required=false)String delete,
			final RedirectAttributes redirectAttributes)
	{
		User user = userService.findByUserName(username);
		String message = null;

		userAttemptsService.resetFailAttempts(username);
		user.setAccountNonLocked(true);
		adminService.enableUser(user);
		message="User account has been unlocked";

		redirectAttributes.addFlashAttribute("message", message);
		return "redirect:/admin/newhome";
	}
	
	@RequestMapping(value="/admin/doregister",method=RequestMethod.POST)
	public String doInternalUserRegister(Model model,@Valid User user, BindingResult result){
		if(result.hasErrors()){
			
			return "Cregister";
		}
			
		if(userService.exists(user.getUsername())){
			result.rejectValue("username", "DuplicateUsernameKey.user.username");
			return "Cregister";
		}
		try{
			user.setEnabled(true);
			user.setAuthority("ROLE_INTERNALUSER");
			userService.save(user);
		}
		catch(DuplicateKeyException ex){
			result.rejectValue("username", "DuplicateUsernameKey.user.username");
			return "Cregister";
			
		}
		
		return "test";
	}
	@RequestMapping(value="/getmessages", method=RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Map<String, Object> getMessages(Principal principal){
		ArrayList<User> users=null;
		if(principal==null){
			users =  new ArrayList<User>(); 	
		}
		else{
			 	
			users = (ArrayList<User>) adminService.getDisabledUser();
			
					}
		
		Map<String, Object> data  = new HashMap<String, Object>();
		data.put("users", users);
		data.put("size", users.size());
		
		return data;
	}
	@RequestMapping(value="/secure/getmessage", method=RequestMethod.POST)
	@ResponseBody
	public String getMessages1(Principal principal){
		System.out.println("Hello World");
		
		return "Hello";
	}
	
	
	@RequestMapping(value="/admin/makerequests", method=RequestMethod.GET)
	public String makeInternalUserRequest(Model model, Principal principal){
		model.addAttribute("internalUserRequest", new InternalUserRequest());
		model.addAttribute("externalUserList", userService.getAllExternalUsers());
	//System.out.println(internalUserRequestService.getListOfApprovedRequests(principal.getName()).get(0).getExternalUsername());
		model.addAttribute("accessList",internalUserRequestService.getListOfApprovedRequests(principal.getName()));
		
		return "makeinternaluserrequest";
	}
	
	@RequestMapping(value="/admin/makerequests", method=RequestMethod.POST)
	public String makeInternalUserRequest(Model model, InternalUserRequest request,
			Principal principal, @RequestParam(value="transactionReview", required=false) String tReview,
			@RequestParam(value="userDetail", required= false) String uDetail,final RedirectAttributes redirectAttributes){
		request.setInternalUsername(principal.getName());
		if(userService.exists(request.getExternalUsername())){
		switch(tReview){
		case "transactionReview": 
			if(internalUserRequestService.exists(request.getInternalUsername(),request.getExternalUsername())){
				request = internalUserRequestService.find(principal.getName(), request.getExternalUsername());
				switch(request.getTransactionalReview()){
				case 1: List<Account> accs = accountService.getAforU(request.getExternalUsername());
						Map<Account, List<Transaction>> map = new HashMap<Account,List< Transaction>>();
						for(Account acc : accs){
						
						if(acc!=null){
						
						
						List<Transaction> t = transService.getTforA(acc.getAccountNumber());
						map.put(acc, t);
						}
						}
				model.addAttribute("map", map);
				return "treview";
				
				case 0: redirectAttributes.addFlashAttribute("error", "your request for Transaction Review has already been updated..Please wait for user response");
						break;
				case -1: request.setTransactionalReview(0);
				internalUserRequestService.update(request);
				redirectAttributes.addFlashAttribute("message", "your request for Transaction Review has been updated");
							break;
				case 2: redirectAttributes.addFlashAttribute("error", "your request for Transaction Review has denied by user");
						break;
				
				}
			}else {
					request.setTransactionalReview(0);
					internalUserRequestService.storeInternalUserRequest(request);
					redirectAttributes.addFlashAttribute("message", "your request for Transaction Review has been sent");
						}
		break;
		
		case "userDetail" : 
			if(internalUserRequestService.exists(request.getInternalUsername(),request.getExternalUsername())){
			request = internalUserRequestService.find(principal.getName(), request.getExternalUsername());
			
			if(request.getUserDetails() == 1){
				return "test2";
			}
			else if(request.getUserDetails() == 0){
				redirectAttributes.addFlashAttribute("error", "your request for User Detail has already been updated..Please wait for user response");
			}else{
			request.setUserDetails(0);
			internalUserRequestService.update(request);
			redirectAttributes.addFlashAttribute("message", "your request for User Detail has been updated");
			}}else {
				request.setUserDetails(0);
				internalUserRequestService.storeInternalUserRequest(request);
				redirectAttributes.addFlashAttribute("message", "your request for User Detail has been sent");
					}
		break;
		
		case "accountDetail" : if(internalUserRequestService.exists(request.getInternalUsername(),request.getExternalUsername())){
			request = internalUserRequestService.find(principal.getName(), request.getExternalUsername());
			
			if(request.getAccountDetails() == 1){
				return "test2";
			}else if(request.getAccountDetails() == 0){
				redirectAttributes.addFlashAttribute("error", "your request for Account Details has already been updated..Please wait for user response");
			}else{
			request.setAccountDetails(0);
			internalUserRequestService.update(request);
			redirectAttributes.addFlashAttribute("message", "your request for Account Details has been updated");
			}}else {
				request.setAccountDetails(0);
				internalUserRequestService.storeInternalUserRequest(request);
				redirectAttributes.addFlashAttribute("message", "your request for Account Details has been sent");
					}
		break;
		}
		
		}else{
			redirectAttributes.addFlashAttribute("error", "select a valid username");
		}

		
		return "redirect:/admin/makerequests";
		
		
	}
	
}

